-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Anamakine: localhost:3306
-- Üretim Zamanı: 22 Şub 2022, 14:37:16
-- Sunucu sürümü: 5.7.24
-- PHP Sürümü: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `sertifikadenizi`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `agreement` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `courses`
--

INSERT INTO `courses` (`id`, `name`, `slug`, `description`, `agreement`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Eğitici Eğitimi', 'egitici-egitimi', '<p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(21, 21, 21); background: rgb(252, 252, 252); font-size: medium;\">EĞİTİCİNİN EĞİTİMİ NEDİR?<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"color: rgb(21, 21, 21); background: rgb(252, 252, 252); font-size: medium;\"><br></span></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"font-size: medium;\"><span style=\"color: rgb(21, 21, 21); background: rgb(252, 252, 252);\"><o:p></o:p></span><span style=\"background-color: rgb(252, 252, 252); color: rgb(21, 21, 21);\">Bu eğitimimiz, uzman olduğu konuda eğitmen olmak isteyen veya mevcut işinde zaman zaman eğitim vermek durumunda olan bireylerin eğitim sürecini tasarlayıp yönetebilmesini ve nitelikli bir eğitici olmasını sağlamayı hedeflemektedir.</span></span></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"font-size: medium;\"><span style=\"color: rgb(21, 21, 21); background: rgb(252, 252, 252);\">Ayrıca etkin sunum yapmak ve topluluk önünde konuşmak isteyen kişilere bilgi, beceri ve bakış açısı kazandırmayı da amaçlamaktadır. Modern çağ insanının ilgisini çekecek,motivasyonunu diri tutacak, çağdaş eğitim yöntem ve araçlarını işlevsel şekilde kullanabilecek, iletişimi yüksek, sınıf yönetimi güçlü ve öğretme becerisi üst düzey eğitmenler yetiştirmek amacıyla hazırlanmış bir programdır.</span><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><br></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"font-weight: bolder;\"><span style=\"font-size: 12pt; line-height: 17.12px;\"></span></span><span style=\"font-size: 12pt; line-height: 17.12px;\">Eğitici Eğitimi katılım sağlamak isteyen bireyler için eğitim kurumumuzda pek çok avantaj bulunmaktadır. Öncelikle&nbsp;<span style=\"font-weight: bolder;\">Eğitici Eğitimi online eğitim</span>&nbsp;sayesinde Türkiye’nin her yerinden kurslarımıza katılım gösterebilirsiniz. Bunun için sistemimize kayıt olmak yeterlidir. Eğitim süresi boyunca derslere kaliteli şekilde katılım gösteren kursiyerlerimizin insan kaynakları ve eğitimleri konusunda kendilerini geliştirmeleri mümkündür.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"font-size: 12pt; line-height: 17.12px;\"><br></span></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><span lang=\"ES-TRAD\" style=\"font-size: 12pt; line-height: 17.12px;\">Eğitimimiz 45 saat sürecektir. Eğitim sonunda başarılı olan kişiler MEB onaylı&nbsp;<span style=\"font-weight: bolder;\">Eğitici Eğitimi Sertifikası</span>&nbsp;alacaktır.</span><span style=\"font-size: 12pt; line-height: 17.12px;\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><span lang=\"ES-TRAD\" style=\"font-size: 12pt; line-height: 17.12px;\"><br></span></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"font-size: 12pt; line-height: 17.12px;\"></span></p><p class=\"MsoNormal\" style=\"line-height: 28px; color: rgb(108, 117, 125); font-family: Gotham, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><span style=\"font-size: 12pt; line-height: 17.12px;\">Kursumuzun eğitim sürecinin tamamlanması,yapılan sınavlardan sonra netice vermektedir. Sınavlar, özellikle uzaktan eğitim alan kursiyerlerimiz için online şekilde yapılmaktadır. Sınavdan geçilmesi, mevzuatlara göre sertifika almaları gereken kursiyerlerimizin büyük oranda işine yaramaktadır. Çünkü eğitim kurumumuz&nbsp;<span style=\"font-weight: bolder;\">MEB onaylı</span>&nbsp;sertifikasını sınavları geçen her katılımcısına vermektedir.</span></p>', '<p><br></p>', '2020-11-24 14:05:11', '2020-12-15 07:54:24', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `exams`
--

CREATE TABLE `exams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `begin_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `exams`
--

INSERT INTO `exams` (`id`, `name`, `percentage`, `duration`, `course_id`, `created_at`, `updated_at`, `begin_date`, `end_date`, `status`, `deleted_at`) VALUES
(6, 'Eğiticinin Eğitimi', 60, 25, 1, '2020-12-17 13:34:23', '2022-02-22 14:11:46', '2020-12-19', '2020-12-19', 0, '2022-02-22 14:11:46'),
(10, 'Eğiticinin Eğitimi', 60, 30, 1, '2021-04-16 11:35:54', '2022-02-22 14:11:44', '2021-05-15', '2021-05-16', 0, '2022-02-22 14:11:44'),
(11, 'test', 60, 60, 1, '2022-02-22 14:11:16', '2022-02-22 14:11:16', '2022-02-22', '2022-02-23', 1, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `exam_answers`
--

CREATE TABLE `exam_answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exam_question_id` bigint(20) UNSIGNED NOT NULL,
  `answer_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_true` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `exam_answers`
--

INSERT INTO `exam_answers` (`id`, `exam_question_id`, `answer_text`, `is_true`, `created_at`, `updated_at`) VALUES
(113, 29, 'İstek - Beceri', 0, '2020-12-17 13:36:09', '2020-12-17 13:36:09'),
(114, 29, 'İtiraz - Bahane üretme', 0, '2020-12-17 13:36:09', '2020-12-17 13:36:09'),
(115, 29, 'Eğitim - Öğrenme - Yetişkin', 1, '2020-12-17 13:36:09', '2020-12-17 13:36:09'),
(116, 29, 'Strateji - Kurgu - Analitik düşünme', 0, '2020-12-17 13:36:09', '2020-12-17 13:36:09'),
(117, 30, 'Çeşitli öğretim yöntem ve teknikleri uygulanmalıdır', 0, '2020-12-17 13:37:05', '2020-12-17 13:37:05'),
(118, 30, 'Bilgiler basitten karmaşığa doğru verilmelidir', 0, '2020-12-17 13:37:05', '2020-12-17 13:37:05'),
(119, 30, 'Olumlu ve iyi bir eğitim ortamı içinde olmalıdır', 0, '2020-12-17 13:37:05', '2020-12-17 13:37:05'),
(120, 30, 'Eğitim süresince isteğe bağlı katılmalıdır', 1, '2020-12-17 13:37:05', '2020-12-17 13:37:05'),
(121, 31, 'Deneysel metod', 0, '2020-12-17 13:37:55', '2020-12-17 13:37:55'),
(122, 31, 'Doğaçlama eğitim metodu', 1, '2020-12-17 13:37:55', '2020-12-17 13:37:55'),
(123, 31, 'Kendi kendine ders metodu', 0, '2020-12-17 13:37:55', '2020-12-17 13:37:55'),
(124, 31, 'Atölye çalışması', 0, '2020-12-17 13:37:55', '2020-12-17 13:37:55'),
(125, 32, 'Tahta, Tepegöz', 0, '2020-12-17 13:48:18', '2020-12-17 13:48:18'),
(126, 32, 'Kalem, kağıt, silgi', 1, '2020-12-17 13:48:18', '2020-12-17 13:48:18'),
(127, 32, 'Konuşmacı kartı, posterler, nesneler', 0, '2020-12-17 13:48:18', '2020-12-17 13:48:18'),
(128, 32, 'Projeksiyon', 0, '2020-12-17 13:48:18', '2020-12-17 13:48:18'),
(129, 33, 'Sayısız eğitim vermiş olmaları', 0, '2020-12-17 13:49:17', '2020-12-17 13:49:17'),
(130, 33, 'İyi bir insan olmaları', 0, '2020-12-17 13:49:17', '2020-12-17 13:49:17'),
(131, 33, 'Özgüven, beden dili, konuya hakimiyetleri', 1, '2020-12-17 13:49:17', '2020-12-17 13:49:17'),
(132, 33, 'Ünlü olmaları', 0, '2020-12-17 13:49:17', '2020-12-17 13:49:17'),
(133, 34, '55 beden dili, 38 ses tonu, 7 kelimeler', 1, '2020-12-17 13:50:04', '2020-12-17 13:50:04'),
(134, 34, '20 beden dili, 68 ses tonu, 12 kelimeler', 0, '2020-12-17 13:50:04', '2020-12-17 13:50:04'),
(135, 34, '40 beden dili, 40 ses tonu,  20  kelimeler', 0, '2020-12-17 13:50:04', '2020-12-17 13:50:04'),
(136, 34, '84 beden dili, 9 ses tonu, 7 kelimeler', 0, '2020-12-17 13:50:04', '2020-12-17 13:50:04'),
(137, 35, 'Dünyanın en dürüst canlısı insan bedenidir', 0, '2020-12-17 13:51:10', '2020-12-17 13:51:10'),
(138, 35, 'İnsanlar gördüklerine duyduklarından daha fazla inanır', 0, '2020-12-17 13:51:10', '2020-12-17 13:51:10'),
(139, 35, 'Gerek yok her sözü beyana, bir bakış bin söz eder anlayana', 0, '2020-12-17 13:51:10', '2020-12-17 13:51:10'),
(140, 35, 'Ne söylediğiniz değil, nasıl söylediğiniz önemlidir', 1, '2020-12-17 13:51:10', '2020-12-17 13:51:10'),
(141, 36, 'Avusturalya', 0, '2020-12-17 13:52:03', '2020-12-17 13:52:03'),
(142, 36, 'Kanada', 0, '2020-12-17 13:52:03', '2020-12-17 13:52:03'),
(143, 36, 'Amerika', 1, '2020-12-17 13:52:03', '2020-12-17 13:52:03'),
(144, 36, 'Türkiye', 0, '2020-12-17 13:52:03', '2020-12-17 13:52:03'),
(145, 37, 'Mimiklerini kullanmadıklarından onlar için nasıl gidiyor anlayamazsınız.', 0, '2020-12-17 13:53:13', '2020-12-17 13:53:13'),
(146, 37, 'Sabırsızdır ve hemen sonuca ulaşmak isterler', 1, '2020-12-17 13:53:13', '2020-12-17 13:53:13'),
(147, 37, 'Kendi doğrularını mutlak doğru kabul ederler', 0, '2020-12-17 13:53:13', '2020-12-17 13:53:13'),
(148, 37, 'Sinerjiyi düşürür, geri bildirimde bulunmazlar', 0, '2020-12-17 13:53:13', '2020-12-17 13:53:13'),
(149, 38, 'Ne? Neden? Nerede? Ne zaman? Nasıl? Kiminle', 1, '2020-12-17 13:53:58', '2020-12-17 13:53:58'),
(150, 38, 'Kim? Kime? Kiminle? Ne zaman? Nerede?', 0, '2020-12-17 13:53:58', '2020-12-17 13:53:58'),
(151, 38, 'Kim? Neden? Niye? Niçin? Nasıl? Ne zaman?', 0, '2020-12-17 13:53:58', '2020-12-17 13:53:58'),
(152, 38, 'Hepsi', 0, '2020-12-17 13:53:58', '2020-12-17 13:53:58'),
(153, 39, 'Sunumun önemi', 0, '2020-12-17 13:54:51', '2020-12-17 13:54:51'),
(154, 39, 'Beden dilinin önemi', 0, '2020-12-17 13:54:51', '2020-12-17 13:54:51'),
(155, 39, 'Pedagojik ve Androgojik', 1, '2020-12-17 13:54:51', '2020-12-17 13:54:51'),
(156, 39, 'Zaman kavramı', 0, '2020-12-17 13:54:51', '2020-12-17 13:54:51'),
(157, 40, 'Katılımcıların artmasını sağlar', 0, '2020-12-17 13:56:32', '2020-12-17 13:56:32'),
(158, 40, 'Dikkat çeker ve dinleyicileri heyecanlandırır', 1, '2020-12-17 13:56:32', '2020-12-17 13:56:32'),
(159, 40, 'Seminer uzar', 0, '2020-12-17 13:56:32', '2020-12-17 13:56:32'),
(160, 40, 'Katılımcılar sıkılır', 0, '2020-12-17 13:56:32', '2020-12-17 13:56:32'),
(161, 41, 'Karmaşık, anlaşılması zor', 0, '2020-12-17 13:58:17', '2020-12-17 13:58:17'),
(162, 41, 'Broşür ve dergi dağıtıp gelen soruları yanıltmak', 0, '2020-12-17 13:58:17', '2020-12-17 13:58:17'),
(163, 41, 'Tane tane, dikkatli, güncel ve esprili', 1, '2020-12-17 13:58:17', '2020-12-17 13:58:17'),
(164, 41, 'Soru cevap şekli', 0, '2020-12-17 13:58:17', '2020-12-17 13:58:17'),
(165, 42, 'İkili tartışma grubu metodu', 0, '2020-12-17 14:00:04', '2020-12-17 14:00:04'),
(166, 42, 'Anahtar kelime metodu', 0, '2020-12-17 14:00:04', '2020-12-17 14:00:04'),
(167, 42, 'Deneysel metod', 0, '2020-12-17 14:00:04', '2020-12-17 14:00:04'),
(168, 42, 'Hepsi', 1, '2020-12-17 14:00:04', '2020-12-17 14:00:04'),
(169, 43, 'Sunum - Hazırlama', 0, '2020-12-17 14:01:22', '2020-12-17 14:01:22'),
(170, 43, 'Analiz - Düşünme', 0, '2020-12-17 14:01:22', '2020-12-17 14:01:22'),
(171, 43, 'Planlama - Malzeme seçme', 0, '2020-12-17 14:01:22', '2020-12-17 14:01:22'),
(172, 43, 'Hepsi', 1, '2020-12-17 14:01:22', '2020-12-17 14:01:22'),
(293, 74, 'İstek - Beceri', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(294, 74, 'İtiraz - Bahane üretme', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(295, 74, 'Eğitim - Öğrenme - Yetişkin', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(296, 74, 'Strateji - Kurgu - Analitik düşünme', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(297, 75, 'Çeşitli öğretim yöntem ve teknikleri uygulanmalıdır', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(298, 75, 'Bilgiler basitten karmaşığa doğru verilmelidir', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(299, 75, 'Olumlu ve iyi bir eğitim ortamı içinde olmalıdır', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(300, 75, 'Eğitim süresince isteğe bağlı katılmalıdır', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(301, 76, 'Deneysel metod', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(302, 76, 'Doğaçlama eğitim metodu', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(303, 76, 'Kendi kendine ders metodu', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(304, 76, 'Atölye çalışması', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(305, 77, 'Tahta, Tepegöz', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(306, 77, 'Kalem, kağıt, silgi', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(307, 77, 'Konuşmacı kartı, posterler, nesneler', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(308, 77, 'Projeksiyon', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(309, 78, 'Sayısız eğitim vermiş olmaları', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(310, 78, 'İyi bir insan olmaları', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(311, 78, 'Özgüven, beden dili, konuya hakimiyetleri', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(312, 78, 'Ünlü olmaları', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(313, 79, '55 beden dili, 38 ses tonu, 7 kelimeler', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(314, 79, '20 beden dili, 68 ses tonu, 12 kelimeler', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(315, 79, '40 beden dili, 40 ses tonu,  20  kelimeler', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(316, 79, '84 beden dili, 9 ses tonu, 7 kelimeler', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(317, 80, 'Dünyanın en dürüst canlısı insan bedenidir', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(318, 80, 'İnsanlar gördüklerine duyduklarından daha fazla inanır', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(319, 80, 'Gerek yok her sözü beyana, bir bakış bin söz eder anlayana', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(320, 80, 'Ne söylediğiniz değil, nasıl söylediğiniz önemlidir', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(321, 81, 'Avusturalya', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(322, 81, 'Kanada', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(323, 81, 'Amerika', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(324, 81, 'Türkiye', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(325, 82, 'Mimiklerini kullanmadıklarından onlar için nasıl gidiyor anlayamazsınız.', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(326, 82, 'Sabırsızdır ve hemen sonuca ulaşmak isterler', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(327, 82, 'Kendi doğrularını mutlak doğru kabul ederler', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(328, 82, 'Sinerjiyi düşürür, geri bildirimde bulunmazlar', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(329, 83, 'Ne? Neden? Nerede? Ne zaman? Nasıl? Kiminle', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(330, 83, 'Kim? Kime? Kiminle? Ne zaman? Nerede?', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(331, 83, 'Kim? Neden? Niye? Niçin? Nasıl? Ne zaman?', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(332, 83, 'Hepsi', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(333, 84, 'Sunumun önemi', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(334, 84, 'Beden dilinin önemi', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(335, 84, 'Pedagojik ve Androgojik', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(336, 84, 'Zaman kavramı', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(337, 85, 'Katılımcıların artmasını sağlar', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(338, 85, 'Dikkat çeker ve dinleyicileri heyecanlandırır', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(339, 85, 'Seminer uzar', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(340, 85, 'Katılımcılar sıkılır', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(341, 86, 'Karmaşık, anlaşılması zor', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(342, 86, 'Broşür ve dergi dağıtıp gelen soruları yanıltmak', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(343, 86, 'Tane tane, dikkatli, güncel ve esprili', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(344, 86, 'Soru cevap şekli', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(345, 87, 'İkili tartışma grubu metodu', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(346, 87, 'Anahtar kelime metodu', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(347, 87, 'Deneysel metod', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(348, 87, 'Hepsi', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(349, 88, 'Sunum - Hazırlama', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(350, 88, 'Analiz - Düşünme', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(351, 88, 'Planlama - Malzeme seçme', 0, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(352, 88, 'Hepsi', 1, '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(353, 89, 'İstek - Beceri', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(354, 89, 'İtiraz - Bahane üretme', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(355, 89, 'Eğitim - Öğrenme - Yetişkin', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(356, 89, 'Strateji - Kurgu - Analitik düşünme', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(357, 90, 'Çeşitli öğretim yöntem ve teknikleri uygulanmalıdır', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(358, 90, 'Bilgiler basitten karmaşığa doğru verilmelidir', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(359, 90, 'Olumlu ve iyi bir eğitim ortamı içinde olmalıdır', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(360, 90, 'Eğitim süresince isteğe bağlı katılmalıdır', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(361, 91, 'Deneysel metod', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(362, 91, 'Doğaçlama eğitim metodu', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(363, 91, 'Kendi kendine ders metodu', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(364, 91, 'Atölye çalışması', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(365, 92, 'Tahta, Tepegöz', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(366, 92, 'Kalem, kağıt, silgi', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(367, 92, 'Konuşmacı kartı, posterler, nesneler', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(368, 92, 'Projeksiyon', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(369, 93, 'Sayısız eğitim vermiş olmaları', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(370, 93, 'İyi bir insan olmaları', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(371, 93, 'Özgüven, beden dili, konuya hakimiyetleri', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(372, 93, 'Ünlü olmaları', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(373, 94, '55 beden dili, 38 ses tonu, 7 kelimeler', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(374, 94, '20 beden dili, 68 ses tonu, 12 kelimeler', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(375, 94, '40 beden dili, 40 ses tonu,  20  kelimeler', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(376, 94, '84 beden dili, 9 ses tonu, 7 kelimeler', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(377, 95, 'Dünyanın en dürüst canlısı insan bedenidir', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(378, 95, 'İnsanlar gördüklerine duyduklarından daha fazla inanır', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(379, 95, 'Gerek yok her sözü beyana, bir bakış bin söz eder anlayana', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(380, 95, 'Ne söylediğiniz değil, nasıl söylediğiniz önemlidir', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(381, 96, 'Avusturalya', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(382, 96, 'Kanada', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(383, 96, 'Amerika', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(384, 96, 'Türkiye', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(385, 97, 'Mimiklerini kullanmadıklarından onlar için nasıl gidiyor anlayamazsınız.', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(386, 97, 'Sabırsızdır ve hemen sonuca ulaşmak isterler', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(387, 97, 'Kendi doğrularını mutlak doğru kabul ederler', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(388, 97, 'Sinerjiyi düşürür, geri bildirimde bulunmazlar', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(389, 98, 'Ne? Neden? Nerede? Ne zaman? Nasıl? Kiminle', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(390, 98, 'Kim? Kime? Kiminle? Ne zaman? Nerede?', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(391, 98, 'Kim? Neden? Niye? Niçin? Nasıl? Ne zaman?', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(392, 98, 'Hepsi', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(393, 99, 'Sunumun önemi', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(394, 99, 'Beden dilinin önemi', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(395, 99, 'Pedagojik ve Androgojik', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(396, 99, 'Zaman kavramı', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(397, 100, 'Katılımcıların artmasını sağlar', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(398, 100, 'Dikkat çeker ve dinleyicileri heyecanlandırır', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(399, 100, 'Seminer uzar', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(400, 100, 'Katılımcılar sıkılır', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(401, 101, 'Karmaşık, anlaşılması zor', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(402, 101, 'Broşür ve dergi dağıtıp gelen soruları yanıltmak', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(403, 101, 'Tane tane, dikkatli, güncel ve esprili', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(404, 101, 'Soru cevap şekli', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(405, 102, 'İkili tartışma grubu metodu', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(406, 102, 'Anahtar kelime metodu', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(407, 102, 'Deneysel metod', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(408, 102, 'Hepsi', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(409, 103, 'Sunum - Hazırlama', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(410, 103, 'Analiz - Düşünme', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(411, 103, 'Planlama - Malzeme seçme', 0, '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(412, 103, 'Hepsi', 1, '2022-02-22 14:11:27', '2022-02-22 14:11:27');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `exam_instances`
--

CREATE TABLE `exam_instances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `exam_instances`
--

INSERT INTO `exam_instances` (`id`, `exam_id`, `status`, `created_at`, `updated_at`) VALUES
(10, 6, 1, '2020-12-19 07:08:00', '2020-12-19 07:08:00'),
(24, 10, 0, '2021-05-10 18:02:00', '2021-05-10 18:02:00'),
(25, 11, 0, '2022-02-22 14:11:20', '2022-02-22 14:11:20');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `exam_questions`
--

CREATE TABLE `exam_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exam_id` bigint(20) UNSIGNED NOT NULL,
  `type` enum('text','image','video') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `exam_questions`
--

INSERT INTO `exam_questions` (`id`, `question_text`, `exam_id`, `type`, `created_at`, `updated_at`) VALUES
(29, 'Temel kavramların tanımı nedir ve hangisidir?', 6, 'text', '2020-12-17 13:36:09', '2020-12-17 13:36:09'),
(30, 'Yetişkin eğitimlerinde öne çıkan detaylardan hangisi yanlıştır?', 6, 'text', '2020-12-17 13:37:05', '2020-12-17 13:37:05'),
(31, 'Eğitim teknikleri metodlarında hangisi yanlıştır?', 6, 'text', '2020-12-17 13:37:55', '2020-12-17 13:37:55'),
(32, 'Etkili bir sunum yaparken ihtiyacınız olan görsel malzemeler ve sunum araçları aşağıdakilerden hangisi değildir?', 6, 'text', '2020-12-17 13:48:18', '2020-12-17 13:48:18'),
(33, 'Dünyanın en iyi konuşmacılarına baktığımızda onlarda ön plana çıkan başarının temel sebepleri aşağıdakilerden hangisidir?', 6, 'text', '2020-12-17 13:49:17', '2020-12-17 13:49:17'),
(34, 'Aşağıdakilerden hangisinde beden dilinin etkileme gücü yüzdeleri doğru belirtilmiştir?', 6, 'text', '2020-12-17 13:50:04', '2020-12-17 13:50:04'),
(35, 'Ünlü eğitimci John Tschohi\'nin birçok eğitimde kullandığı ve beden dilinin önemini anlatan sözü aşağıdakilerden hangisidir?', 6, 'text', '2020-12-17 13:51:10', '2020-12-17 13:51:10'),
(36, 'Dünyada beden dilini en çok kullanan ülke hangisidir?', 6, 'text', '2020-12-17 13:52:03', '2020-12-17 13:52:03'),
(37, 'Kişilik tiplerinden aceleci tavşanın tanımı aşağıdakilerden hangisidir?', 6, 'text', '2020-12-17 13:53:13', '2020-12-17 13:53:13'),
(38, '5N1K kuralının açılımı aşağıdakilerden hangisinde doğru verilmiştir?', 6, 'text', '2020-12-17 13:53:58', '2020-12-17 13:53:58'),
(39, 'Aşağıdakilerden hangisi bireyi yaşam boyu öğrenmeye (life-longeucation) hazırlama bilimidir?', 6, 'text', '2020-12-17 13:54:51', '2020-12-17 13:54:51'),
(40, 'Yetişkin eğitimlerinde görsel ve sayısal verilerin önemi aşağıdakilerin hangisi ile eşleşir?', 6, 'text', '2020-12-17 13:56:32', '2020-12-17 13:56:32'),
(41, 'İyi bir konuşmacının anlatım dili aşağıdakilerden hangisidir?', 6, 'text', '2020-12-17 13:58:17', '2020-12-17 13:58:17'),
(42, 'Aşağıdakilerden hangisi eğitim metodlarındadır?', 6, 'text', '2020-12-17 14:00:04', '2020-12-17 14:00:04'),
(43, 'Aşağıdakilerden hangisi akılda kalıcı kusursuz bir eğitim verebilmeniz için yapılması gerekenlerdir?', 6, 'text', '2020-12-17 14:01:22', '2020-12-17 14:01:22'),
(74, 'Temel kavramların tanımı nedir ve hangisidir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(75, 'Yetişkin eğitimlerinde öne çıkan detaylardan hangisi yanlıştır?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(76, 'Eğitim teknikleri metodlarında hangisi yanlıştır?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(77, 'Etkili bir sunum yaparken ihtiyacınız olan görsel malzemeler ve sunum araçları aşağıdakilerden hangisi değildir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(78, 'Dünyanın en iyi konuşmacılarına baktığımızda onlarda ön plana çıkan başarının temel sebepleri aşağıdakilerden hangisidir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(79, 'Aşağıdakilerden hangisinde beden dilinin etkileme gücü yüzdeleri doğru belirtilmiştir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(80, 'Ünlü eğitimci John Tschohi\'nin birçok eğitimde kullandığı ve beden dilinin önemini anlatan sözü aşağıdakilerden hangisidir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(81, 'Dünyada beden dilini en çok kullanan ülke hangisidir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(82, 'Kişilik tiplerinden aceleci tavşanın tanımı aşağıdakilerden hangisidir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(83, '5N1K kuralının açılımı aşağıdakilerden hangisinde doğru verilmiştir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(84, 'Aşağıdakilerden hangisi bireyi yaşam boyu öğrenmeye (life-longeucation) hazırlama bilimidir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(85, 'Yetişkin eğitimlerinde görsel ve sayısal verilerin önemi aşağıdakilerin hangisi ile eşleşir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(86, 'İyi bir konuşmacının anlatım dili aşağıdakilerden hangisidir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(87, 'Aşağıdakilerden hangisi eğitim metodlarındadır?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(88, 'Aşağıdakilerden hangisi akılda kalıcı kusursuz bir eğitim verebilmeniz için yapılması gerekenlerdir?', 10, 'text', '2021-05-15 14:22:22', '2021-05-15 14:22:22'),
(89, 'Temel kavramların tanımı nedir ve hangisidir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(90, 'Yetişkin eğitimlerinde öne çıkan detaylardan hangisi yanlıştır?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(91, 'Eğitim teknikleri metodlarında hangisi yanlıştır?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(92, 'Etkili bir sunum yaparken ihtiyacınız olan görsel malzemeler ve sunum araçları aşağıdakilerden hangisi değildir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(93, 'Dünyanın en iyi konuşmacılarına baktığımızda onlarda ön plana çıkan başarının temel sebepleri aşağıdakilerden hangisidir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(94, 'Aşağıdakilerden hangisinde beden dilinin etkileme gücü yüzdeleri doğru belirtilmiştir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(95, 'Ünlü eğitimci John Tschohi\'nin birçok eğitimde kullandığı ve beden dilinin önemini anlatan sözü aşağıdakilerden hangisidir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(96, 'Dünyada beden dilini en çok kullanan ülke hangisidir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(97, 'Kişilik tiplerinden aceleci tavşanın tanımı aşağıdakilerden hangisidir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(98, '5N1K kuralının açılımı aşağıdakilerden hangisinde doğru verilmiştir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(99, 'Aşağıdakilerden hangisi bireyi yaşam boyu öğrenmeye (life-longeucation) hazırlama bilimidir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(100, 'Yetişkin eğitimlerinde görsel ve sayısal verilerin önemi aşağıdakilerin hangisi ile eşleşir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(101, 'İyi bir konuşmacının anlatım dili aşağıdakilerden hangisidir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(102, 'Aşağıdakilerden hangisi eğitim metodlarındadır?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27'),
(103, 'Aşağıdakilerden hangisi akılda kalıcı kusursuz bir eğitim verebilmeniz için yapılması gerekenlerdir?', 11, 'text', '2022-02-22 14:11:27', '2022-02-22 14:11:27');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `exam_results`
--

CREATE TABLE `exam_results` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exam_instance_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `true_answer_count` int(11) DEFAULT NULL,
  `success_rate` decimal(8,2) DEFAULT NULL,
  `is_success` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `exam_results`
--

INSERT INTO `exam_results` (`id`, `exam_instance_id`, `user_id`, `true_answer_count`, `success_rate`, `is_success`, `created_at`, `updated_at`, `ip_address`) VALUES
(8, 10, 11, 15, '100.00', 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50', '46.221.221.166'),
(9, 10, 6, 13, '86.67', 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08', '95.10.17.56'),
(10, 10, 16, 11, '73.33', 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42', '88.230.157.42'),
(11, 10, 5, 14, '93.33', 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38', '95.173.242.73'),
(12, 10, 12, 13, '86.67', 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08', '195.142.71.128'),
(13, 10, 8, 14, '93.33', 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06', '151.135.1.117'),
(14, 10, 17, 11, '73.33', 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47', '176.240.226.170'),
(15, 10, 14, 9, '60.00', 1, '2020-12-19 09:26:41', '2020-12-19 09:26:41', '176.240.227.63'),
(16, 10, 10, 14, '93.33', 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41', '78.173.76.229'),
(17, 10, 7, 13, '86.67', 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38', '31.206.199.156'),
(21, 22, 5, 2, '13.33', 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43', '127.0.0.1'),
(19, 10, 9, 11, '73.33', 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11', '176.41.165.129'),
(20, 10, 15, 11, '73.33', 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58', '178.245.76.123'),
(22, 24, 13, 4, '26.67', 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31', '46.196.192.101'),
(23, 24, 19, 14, '93.33', 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18', '5.25.174.194'),
(24, 24, 30, 15, '100.00', 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36', '31.223.49.198'),
(25, 24, 21, 14, '93.33', 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46', '176.54.249.232'),
(26, 24, 27, 12, '80.00', 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44', '88.230.98.156'),
(27, 24, 28, 13, '86.67', 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59', '31.206.31.206'),
(28, 24, 20, 15, '100.00', 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46', '195.155.168.139'),
(29, 24, 23, 15, '100.00', 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38', '195.155.168.139'),
(30, 24, 32, 14, '93.33', 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11', '85.100.64.106'),
(31, 24, 26, 15, '100.00', 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34', '195.155.170.97'),
(32, 24, 29, 14, '93.33', 1, '2021-05-16 10:05:33', '2021-05-16 10:05:34', '78.190.111.65'),
(33, 24, 31, 13, '86.67', 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30', '176.240.168.92'),
(34, 24, 33, 14, '93.33', 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46', '78.167.5.52'),
(35, 24, 22, 12, '80.00', 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21', '176.240.165.49'),
(36, 24, 25, 15, '100.00', 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49', '176.54.204.196'),
(37, 24, 24, 15, '100.00', 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19', '78.173.134.58'),
(38, 25, 2, 0, '0.00', 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21', '127.0.0.1');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `exam_result_details`
--

CREATE TABLE `exam_result_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exam_result_id` bigint(20) UNSIGNED NOT NULL,
  `exam_question_id` bigint(20) UNSIGNED NOT NULL,
  `exam_customer_answer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `actual_true_answer_id` bigint(20) UNSIGNED NOT NULL,
  `is_true` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `exam_result_details`
--

INSERT INTO `exam_result_details` (`id`, `exam_result_id`, `exam_question_id`, `exam_customer_answer_id`, `actual_true_answer_id`, `is_true`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, '2020-11-24 14:11:56', '2020-11-24 14:11:56'),
(2, 2, 1, 2, 1, 0, '2020-11-24 14:13:54', '2020-11-24 14:13:54'),
(3, 3, 2, 5, 8, 0, '2020-12-02 11:20:08', '2020-12-02 11:20:08'),
(4, 3, 3, 11, 11, 1, '2020-12-02 11:20:08', '2020-12-02 11:20:08'),
(5, 3, 4, 15, 15, 1, '2020-12-02 11:20:08', '2020-12-02 11:20:08'),
(6, 3, 5, 20, 20, 1, '2020-12-02 11:20:08', '2020-12-02 11:20:08'),
(7, 3, 6, 23, 23, 1, '2020-12-02 11:20:08', '2020-12-02 11:20:08'),
(8, 3, 7, 26, 26, 1, '2020-12-02 11:20:08', '2020-12-02 11:20:08'),
(9, 3, 8, 29, 29, 1, '2020-12-02 11:20:08', '2020-12-02 11:20:08'),
(10, 3, 9, 33, 33, 1, '2020-12-02 11:20:08', '2020-12-02 11:20:08'),
(11, 3, 10, 38, 38, 1, '2020-12-02 11:20:08', '2020-12-02 11:20:08'),
(12, 3, 11, 43, 43, 1, '2020-12-02 11:20:08', '2020-12-02 11:20:08'),
(13, 4, 2, 8, 8, 1, '2020-12-03 13:30:52', '2020-12-03 13:30:52'),
(14, 4, 3, 11, 11, 1, '2020-12-03 13:30:52', '2020-12-03 13:30:52'),
(15, 4, 4, 15, 15, 1, '2020-12-03 13:30:52', '2020-12-03 13:30:52'),
(16, 4, 5, 20, 20, 1, '2020-12-03 13:30:52', '2020-12-03 13:30:52'),
(17, 4, 6, 23, 23, 1, '2020-12-03 13:30:52', '2020-12-03 13:30:52'),
(18, 4, 7, 26, 26, 1, '2020-12-03 13:30:52', '2020-12-03 13:30:52'),
(19, 4, 8, 29, 29, 1, '2020-12-03 13:30:52', '2020-12-03 13:30:52'),
(20, 4, 9, 33, 33, 1, '2020-12-03 13:30:52', '2020-12-03 13:30:52'),
(21, 4, 10, 38, 38, 1, '2020-12-03 13:30:52', '2020-12-03 13:30:52'),
(22, 4, 11, 43, 43, 1, '2020-12-03 13:30:52', '2020-12-03 13:30:52'),
(23, 5, 2, NULL, 8, 0, '2020-12-03 14:45:56', '2020-12-03 14:45:56'),
(24, 5, 3, NULL, 11, 0, '2020-12-03 14:45:56', '2020-12-03 14:45:56'),
(25, 5, 4, NULL, 15, 0, '2020-12-03 14:45:56', '2020-12-03 14:45:56'),
(26, 5, 5, NULL, 20, 0, '2020-12-03 14:45:56', '2020-12-03 14:45:56'),
(27, 5, 6, NULL, 23, 0, '2020-12-03 14:45:56', '2020-12-03 14:45:56'),
(28, 5, 7, NULL, 26, 0, '2020-12-03 14:45:56', '2020-12-03 14:45:56'),
(29, 5, 8, NULL, 29, 0, '2020-12-03 14:45:56', '2020-12-03 14:45:56'),
(30, 5, 9, NULL, 33, 0, '2020-12-03 14:45:56', '2020-12-03 14:45:56'),
(31, 5, 10, NULL, 38, 0, '2020-12-03 14:45:56', '2020-12-03 14:45:56'),
(32, 5, 11, NULL, 43, 0, '2020-12-03 14:45:56', '2020-12-03 14:45:56'),
(33, 6, 12, NULL, 45, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(34, 6, 13, NULL, 49, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(35, 6, 14, NULL, 54, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(36, 6, 15, NULL, 58, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(37, 6, 16, NULL, 61, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(38, 6, 17, NULL, 65, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(39, 6, 18, NULL, 69, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(40, 6, 19, NULL, 73, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(41, 6, 20, NULL, 78, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(42, 6, 21, NULL, 81, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(43, 6, 22, NULL, 85, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(44, 6, 23, NULL, 90, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(45, 6, 24, NULL, 93, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(46, 6, 25, NULL, 100, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(47, 6, 26, 101, 104, 0, '2020-12-17 10:05:25', '2020-12-17 10:05:25'),
(53, 8, 31, 122, 122, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(52, 8, 30, 120, 120, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(51, 8, 29, 115, 115, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(54, 8, 32, 126, 126, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(55, 8, 33, 131, 131, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(56, 8, 34, 133, 133, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(57, 8, 35, 140, 140, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(58, 8, 36, 143, 143, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(59, 8, 37, 146, 146, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(60, 8, 38, 149, 149, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(61, 8, 39, 155, 155, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(62, 8, 40, 158, 158, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(63, 8, 41, 163, 163, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(64, 8, 42, 168, 168, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(65, 8, 43, 172, 172, 1, '2020-12-19 07:09:50', '2020-12-19 07:09:50'),
(66, 9, 29, 115, 115, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(67, 9, 30, 120, 120, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(68, 9, 31, 122, 122, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(69, 9, 32, 126, 126, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(70, 9, 33, 131, 131, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(71, 9, 34, 133, 133, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(72, 9, 35, 140, 140, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(73, 9, 36, 143, 143, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(74, 9, 37, 146, 146, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(75, 9, 38, 149, 149, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(76, 9, 39, 154, 155, 0, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(77, 9, 40, 158, 158, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(78, 9, 41, 163, 163, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(79, 9, 42, 167, 168, 0, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(80, 9, 43, 172, 172, 1, '2020-12-19 07:39:08', '2020-12-19 07:39:08'),
(81, 10, 29, 116, 115, 0, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(82, 10, 30, 120, 120, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(83, 10, 31, 123, 122, 0, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(84, 10, 32, 126, 126, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(85, 10, 33, 131, 131, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(86, 10, 34, 133, 133, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(87, 10, 35, 137, 140, 0, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(88, 10, 36, 141, 143, 0, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(89, 10, 37, 146, 146, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(90, 10, 38, 149, 149, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(91, 10, 39, 155, 155, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(92, 10, 40, 158, 158, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(93, 10, 41, 163, 163, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(94, 10, 42, 168, 168, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(95, 10, 43, 172, 172, 1, '2020-12-19 07:54:42', '2020-12-19 07:54:42'),
(96, 11, 29, 115, 115, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(97, 11, 30, 120, 120, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(98, 11, 31, 122, 122, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(99, 11, 32, 126, 126, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(100, 11, 33, 131, 131, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(101, 11, 34, 133, 133, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(102, 11, 35, 140, 140, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(103, 11, 36, 143, 143, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(104, 11, 37, 146, 146, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(105, 11, 38, 149, 149, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(106, 11, 39, 155, 155, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(107, 11, 40, 158, 158, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(108, 11, 41, 163, 163, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(109, 11, 42, 165, 168, 0, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(110, 11, 43, 172, 172, 1, '2020-12-19 08:04:38', '2020-12-19 08:04:38'),
(111, 12, 29, 115, 115, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(112, 12, 30, 120, 120, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(113, 12, 31, 121, 122, 0, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(114, 12, 32, 126, 126, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(115, 12, 33, 131, 131, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(116, 12, 34, 133, 133, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(117, 12, 35, 140, 140, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(118, 12, 36, 141, 143, 0, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(119, 12, 37, 146, 146, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(120, 12, 38, 149, 149, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(121, 12, 39, 155, 155, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(122, 12, 40, 158, 158, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(123, 12, 41, 163, 163, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(124, 12, 42, 168, 168, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(125, 12, 43, 172, 172, 1, '2020-12-19 08:33:08', '2020-12-19 08:33:08'),
(126, 13, 29, 115, 115, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(127, 13, 30, 120, 120, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(128, 13, 31, 122, 122, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(129, 13, 32, 126, 126, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(130, 13, 33, 131, 131, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(131, 13, 34, 133, 133, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(132, 13, 35, 140, 140, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(133, 13, 36, 143, 143, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(134, 13, 37, 146, 146, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(135, 13, 38, 149, 149, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(136, 13, 39, 155, 155, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(137, 13, 40, 158, 158, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(138, 13, 41, 163, 163, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(139, 13, 42, 167, 168, 0, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(140, 13, 43, 172, 172, 1, '2020-12-19 08:57:06', '2020-12-19 08:57:06'),
(141, 14, 29, 113, 115, 0, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(142, 14, 30, 117, 120, 0, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(143, 14, 31, 122, 122, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(144, 14, 32, 126, 126, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(145, 14, 33, 131, 131, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(146, 14, 34, 133, 133, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(147, 14, 35, 140, 140, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(148, 14, 36, 141, 143, 0, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(149, 14, 37, 145, 146, 0, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(150, 14, 38, 149, 149, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(151, 14, 39, 155, 155, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(152, 14, 40, 158, 158, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(153, 14, 41, 163, 163, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(154, 14, 42, 168, 168, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(155, 14, 43, 172, 172, 1, '2020-12-19 09:18:47', '2020-12-19 09:18:47'),
(156, 15, 29, 115, 115, 1, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(157, 15, 30, 120, 120, 1, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(158, 15, 31, 123, 122, 0, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(159, 15, 32, 127, 126, 0, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(160, 15, 33, 131, 131, 1, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(161, 15, 34, 135, 133, 0, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(162, 15, 35, 137, 140, 0, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(163, 15, 36, 144, 143, 0, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(164, 15, 37, 146, 146, 1, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(165, 15, 38, 152, 149, 0, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(166, 15, 39, 155, 155, 1, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(167, 15, 40, 158, 158, 1, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(168, 15, 41, 163, 163, 1, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(169, 15, 42, 168, 168, 1, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(170, 15, 43, 172, 172, 1, '2020-12-19 09:26:41', '2020-12-19 09:26:41'),
(171, 16, 29, 115, 115, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(172, 16, 30, 120, 120, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(173, 16, 31, 122, 122, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(174, 16, 32, 126, 126, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(175, 16, 33, 131, 131, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(176, 16, 34, 133, 133, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(177, 16, 35, 140, 140, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(178, 16, 36, 143, 143, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(179, 16, 37, 146, 146, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(180, 16, 38, 149, 149, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(181, 16, 39, 155, 155, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(182, 16, 40, 158, 158, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(183, 16, 41, 163, 163, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(184, 16, 42, 167, 168, 0, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(185, 16, 43, 172, 172, 1, '2020-12-19 09:31:41', '2020-12-19 09:31:41'),
(186, 17, 29, 115, 115, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(187, 17, 30, 120, 120, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(188, 17, 31, 122, 122, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(189, 17, 32, 126, 126, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(190, 17, 33, 131, 131, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(191, 17, 34, 133, 133, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(192, 17, 35, 140, 140, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(193, 17, 36, 142, 143, 0, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(194, 17, 37, 146, 146, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(195, 17, 38, 149, 149, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(196, 17, 39, 155, 155, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(197, 17, 40, 158, 158, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(198, 17, 41, 163, 163, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(199, 17, 42, 168, 168, 1, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(200, 17, 43, 170, 172, 0, '2020-12-19 09:35:38', '2020-12-19 09:35:38'),
(201, 18, 29, 115, 115, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(202, 18, 30, 120, 120, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(203, 18, 31, 122, 122, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(204, 18, 32, 127, 126, 0, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(205, 18, 33, 131, 131, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(206, 18, 34, 133, 133, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(207, 18, 35, 140, 140, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(208, 18, 36, 143, 143, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(209, 18, 37, 147, 146, 0, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(210, 18, 38, 149, 149, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(211, 18, 39, 155, 155, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(212, 18, 40, 158, 158, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(213, 18, 41, 163, 163, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(214, 18, 42, 168, 168, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(215, 18, 43, 172, 172, 1, '2020-12-19 10:08:04', '2020-12-19 10:08:04'),
(216, 19, 29, 115, 115, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(217, 19, 30, 120, 120, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(218, 19, 31, 123, 122, 0, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(219, 19, 32, 126, 126, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(220, 19, 33, 131, 131, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(221, 19, 34, 133, 133, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(222, 19, 35, 140, 140, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(223, 19, 36, 143, 143, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(224, 19, 37, 145, 146, 0, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(225, 19, 38, 149, 149, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(226, 19, 39, 155, 155, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(227, 19, 40, 157, 158, 0, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(228, 19, 41, 163, 163, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(229, 19, 42, 167, 168, 0, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(230, 19, 43, 172, 172, 1, '2020-12-19 10:20:11', '2020-12-19 10:20:11'),
(231, 20, 29, 116, 115, 0, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(232, 20, 30, 120, 120, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(233, 20, 31, 122, 122, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(234, 20, 32, 126, 126, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(235, 20, 33, 131, 131, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(236, 20, 34, 135, 133, 0, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(237, 20, 35, 140, 140, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(238, 20, 36, 142, 143, 0, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(239, 20, 37, 146, 146, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(240, 20, 38, 149, 149, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(241, 20, 39, 156, 155, 0, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(242, 20, 40, 158, 158, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(243, 20, 41, 163, 163, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(244, 20, 42, 168, 168, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(245, 20, 43, 172, 172, 1, '2020-12-19 10:59:58', '2020-12-19 10:59:58'),
(246, 21, 44, 173, 175, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(247, 21, 45, 177, 180, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(248, 21, 46, 181, 182, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(249, 21, 47, 185, 186, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(250, 21, 48, 189, 191, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(251, 21, 49, 193, 193, 1, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(252, 21, 50, 197, 200, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(253, 21, 51, 201, 203, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(254, 21, 52, 205, 206, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(255, 21, 53, 209, 209, 1, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(256, 21, 54, 213, 215, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(257, 21, 55, 217, 218, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(258, 21, 56, 221, 223, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(259, 21, 57, 225, 228, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(260, 21, 58, 229, 232, 0, '2020-12-22 13:20:43', '2020-12-22 13:20:43'),
(261, 22, 74, 294, 295, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(262, 22, 75, 298, 300, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(263, 22, 76, 301, 302, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(264, 22, 77, 308, 306, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(265, 22, 78, 309, 311, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(266, 22, 79, 316, 313, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(267, 22, 80, 318, 320, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(268, 22, 81, 324, 323, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(269, 22, 82, 326, 326, 1, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(270, 22, 83, 331, 329, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(271, 22, 84, 336, 335, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(272, 22, 85, 338, 338, 1, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(273, 22, 86, 343, 343, 1, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(274, 22, 87, 348, 348, 1, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(275, 22, 88, 349, 352, 0, '2021-05-15 14:29:31', '2021-05-15 14:29:31'),
(276, 23, 74, 295, 295, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(277, 23, 75, 300, 300, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(278, 23, 76, 302, 302, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(279, 23, 77, 306, 306, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(280, 23, 78, 311, 311, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(281, 23, 79, 313, 313, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(282, 23, 80, 320, 320, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(283, 23, 81, 323, 323, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(284, 23, 82, 326, 326, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(285, 23, 83, 329, 329, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(286, 23, 84, 335, 335, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(287, 23, 85, 338, 338, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(288, 23, 86, 343, 343, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(289, 23, 87, 346, 348, 0, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(290, 23, 88, 352, 352, 1, '2021-05-15 15:27:18', '2021-05-15 15:27:18'),
(291, 24, 74, 295, 295, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(292, 24, 75, 300, 300, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(293, 24, 76, 302, 302, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(294, 24, 77, 306, 306, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(295, 24, 78, 311, 311, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(296, 24, 79, 313, 313, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(297, 24, 80, 320, 320, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(298, 24, 81, 323, 323, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(299, 24, 82, 326, 326, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(300, 24, 83, 329, 329, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(301, 24, 84, 335, 335, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(302, 24, 85, 338, 338, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(303, 24, 86, 343, 343, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(304, 24, 87, 348, 348, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(305, 24, 88, 352, 352, 1, '2021-05-15 17:08:36', '2021-05-15 17:08:36'),
(306, 25, 74, 295, 295, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(307, 25, 75, 300, 300, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(308, 25, 76, 302, 302, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(309, 25, 77, 306, 306, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(310, 25, 78, 311, 311, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(311, 25, 79, 313, 313, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(312, 25, 80, 320, 320, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(313, 25, 81, 323, 323, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(314, 25, 82, 326, 326, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(315, 25, 83, 329, 329, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(316, 25, 84, 335, 335, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(317, 25, 85, 338, 338, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(318, 25, 86, 343, 343, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(319, 25, 87, 347, 348, 0, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(320, 25, 88, 352, 352, 1, '2021-05-15 17:31:46', '2021-05-15 17:31:46'),
(321, 26, 74, 295, 295, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(322, 26, 75, 300, 300, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(323, 26, 76, 303, 302, 0, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(324, 26, 77, 306, 306, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(325, 26, 78, 311, 311, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(326, 26, 79, 316, 313, 0, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(327, 26, 80, 320, 320, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(328, 26, 81, 323, 323, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(329, 26, 82, 326, 326, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(330, 26, 83, 329, 329, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(331, 26, 84, 335, 335, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(332, 26, 85, 338, 338, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(333, 26, 86, 343, 343, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(334, 26, 87, 347, 348, 0, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(335, 26, 88, 352, 352, 1, '2021-05-15 21:13:44', '2021-05-15 21:13:44'),
(336, 27, 74, 295, 295, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(337, 27, 75, 300, 300, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(338, 27, 76, 302, 302, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(339, 27, 77, 306, 306, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(340, 27, 78, 311, 311, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(341, 27, 79, 313, 313, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(342, 27, 80, 320, 320, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(343, 27, 81, 323, 323, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(344, 27, 82, 326, 326, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(345, 27, 83, 329, 329, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(346, 27, 84, 335, 335, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(347, 27, 85, 338, 338, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(348, 27, 86, 344, 343, 0, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(349, 27, 87, 347, 348, 0, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(350, 27, 88, 352, 352, 1, '2021-05-15 21:31:59', '2021-05-15 21:31:59'),
(351, 28, 74, 295, 295, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(352, 28, 75, 300, 300, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(353, 28, 76, 302, 302, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(354, 28, 77, 306, 306, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(355, 28, 78, 311, 311, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(356, 28, 79, 313, 313, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(357, 28, 80, 320, 320, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(358, 28, 81, 323, 323, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(359, 28, 82, 326, 326, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(360, 28, 83, 329, 329, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(361, 28, 84, 335, 335, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(362, 28, 85, 338, 338, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(363, 28, 86, 343, 343, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(364, 28, 87, 348, 348, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(365, 28, 88, 352, 352, 1, '2021-05-15 22:57:46', '2021-05-15 22:57:46'),
(366, 29, 74, 295, 295, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(367, 29, 75, 300, 300, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(368, 29, 76, 302, 302, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(369, 29, 77, 306, 306, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(370, 29, 78, 311, 311, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(371, 29, 79, 313, 313, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(372, 29, 80, 320, 320, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(373, 29, 81, 323, 323, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(374, 29, 82, 326, 326, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(375, 29, 83, 329, 329, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(376, 29, 84, 335, 335, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(377, 29, 85, 338, 338, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(378, 29, 86, 343, 343, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(379, 29, 87, 348, 348, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(380, 29, 88, 352, 352, 1, '2021-05-15 22:59:38', '2021-05-15 22:59:38'),
(381, 30, 74, 295, 295, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(382, 30, 75, 300, 300, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(383, 30, 76, 302, 302, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(384, 30, 77, 306, 306, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(385, 30, 78, 311, 311, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(386, 30, 79, 313, 313, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(387, 30, 80, 320, 320, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(388, 30, 81, 323, 323, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(389, 30, 82, 326, 326, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(390, 30, 83, 329, 329, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(391, 30, 84, 335, 335, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(392, 30, 85, 338, 338, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(393, 30, 86, 343, 343, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(394, 30, 87, 347, 348, 0, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(395, 30, 88, 352, 352, 1, '2021-05-16 08:08:11', '2021-05-16 08:08:11'),
(396, 31, 74, 295, 295, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(397, 31, 75, 300, 300, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(398, 31, 76, 302, 302, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(399, 31, 77, 306, 306, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(400, 31, 78, 311, 311, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(401, 31, 79, 313, 313, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(402, 31, 80, 320, 320, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(403, 31, 81, 323, 323, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(404, 31, 82, 326, 326, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(405, 31, 83, 329, 329, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(406, 31, 84, 335, 335, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(407, 31, 85, 338, 338, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(408, 31, 86, 343, 343, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(409, 31, 87, 348, 348, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(410, 31, 88, 352, 352, 1, '2021-05-16 09:23:34', '2021-05-16 09:23:34'),
(411, 32, 74, 295, 295, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(412, 32, 75, 300, 300, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(413, 32, 76, 302, 302, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(414, 32, 77, 306, 306, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(415, 32, 78, 311, 311, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(416, 32, 79, 313, 313, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(417, 32, 80, 320, 320, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(418, 32, 81, 323, 323, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(419, 32, 82, 326, 326, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(420, 32, 83, 329, 329, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(421, 32, 84, 335, 335, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(422, 32, 85, 338, 338, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(423, 32, 86, 343, 343, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(424, 32, 87, 345, 348, 0, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(425, 32, 88, 352, 352, 1, '2021-05-16 10:05:34', '2021-05-16 10:05:34'),
(426, 33, 74, 295, 295, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(427, 33, 75, 300, 300, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(428, 33, 76, 302, 302, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(429, 33, 77, 306, 306, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(430, 33, 78, 311, 311, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(431, 33, 79, 313, 313, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(432, 33, 80, 317, 320, 0, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(433, 33, 81, 323, 323, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(434, 33, 82, 326, 326, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(435, 33, 83, 329, 329, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(436, 33, 84, 335, 335, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(437, 33, 85, 338, 338, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(438, 33, 86, 344, 343, 0, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(439, 33, 87, 348, 348, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(440, 33, 88, 352, 352, 1, '2021-05-16 10:39:30', '2021-05-16 10:39:30'),
(441, 34, 74, 295, 295, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(442, 34, 75, 300, 300, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(443, 34, 76, 302, 302, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(444, 34, 77, 306, 306, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(445, 34, 78, 311, 311, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(446, 34, 79, 315, 313, 0, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(447, 34, 80, 320, 320, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(448, 34, 81, 323, 323, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(449, 34, 82, 326, 326, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(450, 34, 83, 329, 329, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(451, 34, 84, 335, 335, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(452, 34, 85, 338, 338, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(453, 34, 86, 343, 343, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(454, 34, 87, 348, 348, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(455, 34, 88, 352, 352, 1, '2021-05-16 10:46:46', '2021-05-16 10:46:46'),
(456, 35, 74, 295, 295, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(457, 35, 75, 300, 300, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(458, 35, 76, 302, 302, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(459, 35, 77, 306, 306, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(460, 35, 78, 311, 311, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(461, 35, 79, 315, 313, 0, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(462, 35, 80, 320, 320, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(463, 35, 81, 324, 323, 0, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(464, 35, 82, 326, 326, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(465, 35, 83, 331, 329, 0, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(466, 35, 84, 335, 335, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(467, 35, 85, 338, 338, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(468, 35, 86, 343, 343, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(469, 35, 87, 348, 348, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(470, 35, 88, 352, 352, 1, '2021-05-16 11:46:21', '2021-05-16 11:46:21'),
(471, 36, 74, 295, 295, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(472, 36, 75, 300, 300, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(473, 36, 76, 302, 302, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(474, 36, 77, 306, 306, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(475, 36, 78, 311, 311, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(476, 36, 79, 313, 313, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(477, 36, 80, 320, 320, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(478, 36, 81, 323, 323, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(479, 36, 82, 326, 326, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(480, 36, 83, 329, 329, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(481, 36, 84, 335, 335, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(482, 36, 85, 338, 338, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(483, 36, 86, 343, 343, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(484, 36, 87, 348, 348, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(485, 36, 88, 352, 352, 1, '2021-05-16 12:01:49', '2021-05-16 12:01:49'),
(486, 37, 74, 295, 295, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(487, 37, 75, 300, 300, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(488, 37, 76, 302, 302, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(489, 37, 77, 306, 306, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(490, 37, 78, 311, 311, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(491, 37, 79, 313, 313, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(492, 37, 80, 320, 320, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(493, 37, 81, 323, 323, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(494, 37, 82, 326, 326, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(495, 37, 83, 329, 329, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(496, 37, 84, 335, 335, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(497, 37, 85, 338, 338, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(498, 37, 86, 343, 343, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(499, 37, 87, 348, 348, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(500, 37, 88, 352, 352, 1, '2021-05-16 13:30:19', '2021-05-16 13:30:19'),
(501, 38, 89, 354, 355, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(502, 38, 90, 359, 360, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(503, 38, 91, 361, 362, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(504, 38, 92, 367, 366, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(505, 38, 93, 369, 371, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(506, 38, 94, 374, 373, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(507, 38, 95, 378, 380, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(508, 38, 96, 382, 383, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(509, 38, 97, 385, 386, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(510, 38, 98, 391, 389, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(511, 38, 99, 394, 395, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(512, 38, 100, 397, 398, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(513, 38, 101, 402, 403, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(514, 38, 102, 406, 408, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21'),
(515, 38, 103, 409, 412, 0, '2022-02-22 14:12:21', '2022-02-22 14:12:21');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `exam_students`
--

CREATE TABLE `exam_students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exam_instance_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `exam_students`
--

INSERT INTO `exam_students` (`id`, `exam_instance_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 10, 5, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(2, 10, 6, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(3, 10, 7, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(4, 10, 8, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(5, 10, 9, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(6, 10, 10, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(7, 10, 11, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(8, 10, 12, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(9, 10, 14, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(10, 10, 15, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(11, 10, 16, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(12, 10, 17, '2020-12-22 13:03:03', '2020-12-22 13:03:03'),
(17, 24, 13, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(18, 24, 19, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(19, 24, 20, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(20, 24, 21, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(21, 24, 22, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(22, 24, 23, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(23, 24, 24, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(24, 24, 25, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(25, 24, 26, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(26, 24, 27, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(27, 24, 28, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(28, 24, 29, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(29, 24, 30, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(30, 24, 31, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(31, 24, 32, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(32, 24, 33, '2021-05-15 14:22:05', '2021-05-15 14:22:05'),
(33, 25, 2, '2022-02-22 14:11:20', '2022-02-22 14:11:20');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fileable_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileable_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) NOT NULL,
  `extension` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `files`
--

INSERT INTO `files` (`id`, `fileable_type`, `fileable_id`, `name`, `path`, `size`, `extension`, `created_at`, `updated_at`) VALUES
(3, 'App\\Models\\Admin\\Course', 1, '7000cca3c99240b6acff2edfd803676a.jpg', 'JxoCIfvJpsLKPpYc4H6ftGpqiGpGvH6tl78CWTFc.png', 198402, 'png', '2020-12-03 14:41:52', '2020-12-03 14:41:52');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_21_153455_create_files_table', 1),
(5, '2020_11_18_151703_create_permission_tables', 1),
(6, '2020_11_19_094841_create_courses_table', 1),
(7, '2020_11_19_103326_create_videos_table', 1),
(8, '2020_11_19_105942_create_exams_table', 1),
(9, '2020_11_19_120029_create_exam_questions_table', 1),
(10, '2020_11_19_142503_create_exam_answers_table', 1),
(11, '2020_11_19_162632_add_tc_no_to_users_table', 1),
(12, '2020_11_19_181546_create_video_histories_table', 1),
(13, '2020_11_23_104016_add_begin_end_date_to_exams_table', 1),
(14, '2020_11_23_120909_create_exam_instances_table', 1),
(15, '2020_11_23_144416_create_exam_results_table', 1),
(16, '2020_11_23_144931_create_exam_result_details_table', 1),
(17, '2020_11_23_175734_add_status_to_exams_table', 1),
(18, '2020_11_25_145242_create_personal_message_threads_table', 2),
(19, '2020_11_26_172950_create_personal_messages_table', 2),
(20, '2020_12_03_161258_add_ip_address_to_exam_results_table', 3),
(21, '2020_12_17_165710_create_exam_students_table', 4),
(22, '2020_12_22_133114_add_softdeletes', 4);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(1, 'App\\Models\\User', 4),
(2, 'App\\Models\\User', 2),
(2, 'App\\Models\\User', 5),
(2, 'App\\Models\\User', 6),
(2, 'App\\Models\\User', 7),
(2, 'App\\Models\\User', 8),
(2, 'App\\Models\\User', 9),
(2, 'App\\Models\\User', 10),
(2, 'App\\Models\\User', 11),
(2, 'App\\Models\\User', 12),
(2, 'App\\Models\\User', 13),
(2, 'App\\Models\\User', 14),
(2, 'App\\Models\\User', 15),
(2, 'App\\Models\\User', 16),
(2, 'App\\Models\\User', 17),
(2, 'App\\Models\\User', 18),
(2, 'App\\Models\\User', 19),
(2, 'App\\Models\\User', 20),
(2, 'App\\Models\\User', 21),
(2, 'App\\Models\\User', 22),
(2, 'App\\Models\\User', 23),
(2, 'App\\Models\\User', 24),
(2, 'App\\Models\\User', 25),
(2, 'App\\Models\\User', 26),
(2, 'App\\Models\\User', 27),
(2, 'App\\Models\\User', 28),
(2, 'App\\Models\\User', 29),
(2, 'App\\Models\\User', 30),
(2, 'App\\Models\\User', 31),
(2, 'App\\Models\\User', 32),
(2, 'App\\Models\\User', 33);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permission_category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `permission_categories`
--

CREATE TABLE `permission_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `personal_messages`
--

CREATE TABLE `personal_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `thread_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `personal_message_threads`
--

CREATE TABLE `personal_message_threads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sender_id` bigint(20) UNSIGNED NOT NULL,
  `receiver_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `personal_message_threads`
--

INSERT INTO `personal_message_threads` (`id`, `sender_id`, `receiver_id`, `created_at`, `updated_at`) VALUES
(3, 2, 1, '2022-02-22 14:00:06', '2022-02-22 14:00:06');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, '*', '2020-11-24 13:55:55', '2020-11-24 13:55:55'),
(2, 'Üye', NULL, '*', '2020-11-24 13:55:56', '2020-11-24 13:55:56');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tc_no` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `tc_no`, `deleted_at`) VALUES
(1, '.admin', 'admin@sertifikadenizi.com', NULL, '$2y$10$dgd17QpvAP1LmO5pC3TxB.NA5ksX3LefLUvviWVCRmGudy9KBkVLa', 'LzahzKpjGyjAvEj3kQMQGnpUph6NgkHZZPjNCte5wIUOHy07RqB8rQxAUqEi', '2020-11-24 13:55:56', '2020-12-17 13:59:25', NULL, NULL),
(2, '.Demo Üye', 'demo@sertifikadenizi.com', NULL, '$2y$10$zC22ow0rfaY1/hAdj3NqLu3M3omhCnXy8l1tkwFOB3YTS0fjOyp9i', 'h9tRQzJUX1b7RH8YTG4eB5PYTmAsJzarfFMTSli1yvTN1i3IBi4uuCirRM2k', '2020-11-24 13:55:57', '2022-02-22 13:59:42', NULL, NULL),
(4, '.Yağızcan Arslan', 'yagiz@statikyazilim.com.tr', NULL, '$2y$10$/1LuNkNSqP8Lqb5Ca5QWmuvk/TemGdWBjwC3yyIPETeZAySBGjwBa', '6PzycLpdw1pr1jQM8GQ4GNkz0VEhR5KF4o49XZnhq36QSGp5QZ8PGCk0RTgl', '2020-12-03 13:29:59', '2020-12-17 13:58:51', 15847749746, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `videos`
--

INSERT INTO `videos` (`id`, `title`, `link`, `course_id`, `created_at`, `updated_at`) VALUES
(1, 'Eğitici Eğitimi 1. Video', 'https://www.youtube.com/watch?v=16CWP9dmTv0&feature=emb_logo', 1, '2020-11-24 14:08:13', '2020-11-24 15:08:55');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `video_histories`
--

CREATE TABLE `video_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `video_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `video_histories`
--

INSERT INTO `video_histories` (`id`, `video_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-11-24 14:11:37', '2020-11-24 14:11:37'),
(2, 1, 2, '2020-11-24 14:13:24', '2020-11-24 14:13:24'),
(3, 1, 3, '2020-12-02 11:25:55', '2020-12-02 11:25:55');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exams_course_id_foreign` (`course_id`);

--
-- Tablo için indeksler `exam_answers`
--
ALTER TABLE `exam_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exam_answers_exam_question_id_foreign` (`exam_question_id`);

--
-- Tablo için indeksler `exam_instances`
--
ALTER TABLE `exam_instances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exam_instances_exam_id_foreign` (`exam_id`);

--
-- Tablo için indeksler `exam_questions`
--
ALTER TABLE `exam_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exam_questions_exam_id_foreign` (`exam_id`);

--
-- Tablo için indeksler `exam_results`
--
ALTER TABLE `exam_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exam_results_exam_instance_id_foreign` (`exam_instance_id`),
  ADD KEY `exam_results_user_id_foreign` (`user_id`);

--
-- Tablo için indeksler `exam_result_details`
--
ALTER TABLE `exam_result_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exam_result_details_exam_result_id_foreign` (`exam_result_id`),
  ADD KEY `exam_result_details_exam_question_id_foreign` (`exam_question_id`),
  ADD KEY `exam_result_details_exam_customer_answer_id_foreign` (`exam_customer_answer_id`),
  ADD KEY `exam_result_details_actual_true_answer_id_foreign` (`actual_true_answer_id`);

--
-- Tablo için indeksler `exam_students`
--
ALTER TABLE `exam_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exam_students_exam_instance_id_foreign` (`exam_instance_id`),
  ADD KEY `exam_students_user_id_foreign` (`user_id`);

--
-- Tablo için indeksler `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Tablo için indeksler `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_fileable_type_fileable_id_index` (`fileable_type`,`fileable_id`);

--
-- Tablo için indeksler `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Tablo için indeksler `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Tablo için indeksler `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Tablo için indeksler `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_permission_category_id_foreign` (`permission_category_id`);

--
-- Tablo için indeksler `permission_categories`
--
ALTER TABLE `permission_categories`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `personal_messages`
--
ALTER TABLE `personal_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personal_messages_thread_id_foreign` (`thread_id`),
  ADD KEY `personal_messages_user_id_foreign` (`user_id`);

--
-- Tablo için indeksler `personal_message_threads`
--
ALTER TABLE `personal_message_threads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personal_message_threads_sender_id_foreign` (`sender_id`),
  ADD KEY `personal_message_threads_receiver_id_foreign` (`receiver_id`);

--
-- Tablo için indeksler `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Tablo için indeksler `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `videos_course_id_foreign` (`course_id`);

--
-- Tablo için indeksler `video_histories`
--
ALTER TABLE `video_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `video_histories_video_id_foreign` (`video_id`),
  ADD KEY `video_histories_user_id_foreign` (`user_id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `exams`
--
ALTER TABLE `exams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Tablo için AUTO_INCREMENT değeri `exam_answers`
--
ALTER TABLE `exam_answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=413;

--
-- Tablo için AUTO_INCREMENT değeri `exam_instances`
--
ALTER TABLE `exam_instances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Tablo için AUTO_INCREMENT değeri `exam_questions`
--
ALTER TABLE `exam_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- Tablo için AUTO_INCREMENT değeri `exam_results`
--
ALTER TABLE `exam_results`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Tablo için AUTO_INCREMENT değeri `exam_result_details`
--
ALTER TABLE `exam_result_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=516;

--
-- Tablo için AUTO_INCREMENT değeri `exam_students`
--
ALTER TABLE `exam_students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Tablo için AUTO_INCREMENT değeri `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Tablo için AUTO_INCREMENT değeri `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `permission_categories`
--
ALTER TABLE `permission_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `personal_messages`
--
ALTER TABLE `personal_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Tablo için AUTO_INCREMENT değeri `personal_message_threads`
--
ALTER TABLE `personal_message_threads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Tablo için AUTO_INCREMENT değeri `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `video_histories`
--
ALTER TABLE `video_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
