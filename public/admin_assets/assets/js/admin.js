var datatable;
var datatableLanguage = {
        "sDecimal": ",",
        "sEmptyTable": "Tabloda herhangi bir veri mevcut değil",
        "sInfo": "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
        "sInfoEmpty": "Kayıt yok",
        "sInfoFiltered": "(_MAX_ kayıt içerisinden bulunan)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "Sayfada _MENU_ kayıt göster",
        "sLoadingRecords": "Yükleniyor...",
        "sProcessing": "İşleniyor...",
        "sSearch": "Ara:",
        "sZeroRecords": "Eşleşen kayıt bulunamadı",
        "oPaginate": {
            "sFirst": "İlk",
            "sLast": "Son",
            "sNext": "Sonraki",
            "sPrevious": "Önceki"
        },
        "oAria": {
            "sSortAscending": ": artan sütun sıralamasını aktifleştir",
            "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
        },
        "select": {
            "rows": {
                "_": "%d kayıt seçildi",
                "0": "",
                "1": "1 kayıt seçildi"
            }
        }
};
$(document).ready(function () {
    datatable = $('.datatable').DataTable({
        language: datatableLanguage
    });

    $(".summernote").summernote({
        height: 300,
        lang: 'tr-TR',
        tooltip: false
        /*toolbar: [
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link']],
            ['view', ['fullscreen', 'codeview   ', 'undo', 'redo']],
        ]*/
    });
});

function goBack() {
    window.history.back();
}

function deleteItem(e) {
    var actionName = $(e).data("action");
    var subActionName = $(e).data("subaction");
    var id = $(e).data("id");
    $.ajax({
        url: '/admin/dashboard/dashboard.php?action=' + actionName + '&sa=delete',
        type: 'post',
        data: {id: id},
        success: function (data) {
            if (data.success)
                location.reload(true);
            else
                alert("Bir hata oluştu !");
        }
    });
};

function deleteSubItem(e) {
    var url = $(e).data("url"),
        id = $(e).data("id");

    $.ajax({
        url: url,
        type: 'delete',
        data: {id: id},
        success: function (data) {
            if (data.success)
                location.reload(true);
            else
                alert("Bir hata oluştu !");
        }
    });
}
