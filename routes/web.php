<?php

use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\ExamController;
use App\Http\Controllers\Admin\ExamQuestionController;
use App\Http\Controllers\Admin\FilesController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\PersonalMessageController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\VideoController;
use App\Http\Controllers\Admin\ExamInstanceController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\StudentCoursesController;
use App\Http\Controllers\StudentExamController;
use App\Http\Controllers\StudentHomeController;
use App\Http\Controllers\StudentLoginController;
use App\Http\Controllers\StudentPersonalMessageController;
use App\Http\Controllers\StudentVideosController;
use Illuminate\Support\Facades\Route;

// Web Routes
Route::get('/', [HomepageController::class, 'index'])->name('index');
Route::post('contact', [HomepageController::class, 'email'])->name('contact.post');

// Admin Login routes
Route::group(['as' => 'ogrenci.', 'prefix' => 'ogrenci-paneli', 'middleware' => 'guest'], function ()
{
    Route::get('giris', [StudentLoginController::class, 'login'])->name('login');
    Route::post('giris', [StudentLoginController::class, 'check'])->name('login.attempt');
});

Route::get('admin', [HomeController::class, 'index'])->name('admin.index');
// Admin Login routes
Route::group(['as' => 'admin.', 'prefix' => 'admin', 'middleware' => 'guest'], function ()
{
    Route::get('login', [LoginController::class, 'index'])->name('login');
    Route::post('login', [LoginController::class, 'login'])->name('login.attempt');
});

// Student routes
Route::group(['as' => 'ogrenci.', 'prefix' => 'ogrenci-paneli', 'middleware' => 'auth'], function ()
{
    Route::get('', [StudentHomeController::class, 'index'])->name('index');
    Route::get('kurslar', [StudentCoursesController::class, 'index'])->name('courses.index');
    Route::get('kurslar/{course}/video/{video}', [StudentVideosController::class, 'show'])->name('videos.show');

    Route::get('sinavlar', [StudentExamController::class, 'index'])->name('exams.index');
    Route::get('sinavlar/{examInstance}/sonuc', [StudentExamController::class, 'show'])->name('exams.show');
    Route::get('sinavlar/{exam}/katil', [StudentExamController::class, 'join'])->name('exams.join');
    Route::post('sinavlar/bitir', [StudentExamController::class, 'finish'])->name('exams.finish');

    Route::get('soru-sor', [StudentPersonalMessageController::class, 'index'])->name('ask.index');
    Route::post('soru-sor', [StudentPersonalMessageController::class, 'store'])->name('ask.store');

    Route::post('cikis', [StudentLoginController::class, 'logout'])->name('logout');
});

// Admin Routes
Route::group(['as' => 'admin.', 'prefix' => 'admin', 'middleware' => ['auth', 'role:Admin']], function ()
{
    Route::resource('courses', CourseController::class);
    Route::resource('videos', VideoController::class);
    Route::resource('exams', ExamController::class);

    Route::get('exams/{exam}/instance', [ExamInstanceController::class, 'index'])->name('exams.instance.create');
    Route::post('exams/{exam}/instance', [ExamInstanceController::class, 'store'])->name('exams.instance.store');

    Route::post('exams/{exam}/questions/copy', [ExamQuestionController::class, 'copy'])->name('exams.question.copy');
    Route::resource('exams.questions', ExamQuestionController::class);

    Route::resource('users', UserController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('messages', PersonalMessageController::class);
    Route::post('messages/{thread}', [PersonalMessageController::class, 'store'])->name('messages.store');

    Route::get('roles/{role?}/permissions', [RoleController::class, 'permissions'])->name('roles.permissions.view');
    Route::post('roles/{role?}/permissions', [RoleController::class, 'storePermissions'])->name('roles.permissions.store');

    Route::post('upload', [FilesController::class, 'store'])->name('file.upload');
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');
});
