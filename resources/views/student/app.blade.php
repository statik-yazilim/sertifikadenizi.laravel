@include('student.partials.header')

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

@include('student.partials.sidebar')

<!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

        @include('student.partials.topbar')

            <div class="container-fluid">

            @yield('content')

            </div>
        </div>
@include('student.partials.footer')
