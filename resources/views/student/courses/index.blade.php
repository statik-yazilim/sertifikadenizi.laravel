@extends('student.app')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Kurslar</h1>
    <p class="mb-4"></p>
    <div class="card shadow mb-4">
        @foreach ($courses as $course)
            <div class="card-header py-3">
                <h3 class="m-0 font-weight-bold text-secondary">{{ $course->name }}</h3>
            </div>
            <div class="col-md-12 mt-1">
                <div class="row user-education-row box-shadow m-0">
                    {!! $course->description !!}
                </div>
            </div>
       <div class="card-body">
               <div class="modal fade" id="sozlesme{{ $course->id }}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Sözleşme</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {!! $course->agreement !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row user-education-row box-shadow">
                    <div class="col-md-4 col-sm-12">

                        <img id="logo" src="{{ asset('storage/' . $course->files->first()->path) }}" alt=""/>
                        <div class="row mt-4">
                            <div class="col-12 pull-right text-center">
                            </div>
                          {{--  <div class="col-12 text-left mt-2">
                                <a class="btn-video-play pull-left d-block" href="#" data-toggle="modal" data-target="#sozlesme{{ $course->id }}"> Sözleşme <i class="fa
                                fa-file-text-o"></i></a>
                            </div>--}}
                        </div>
                    </div>
                    @if ($course->videos->isNotEmpty())
                        <div class="col-md-8 col-sm-12">
                            <h3>
                            <span class="video-completed-count pull-right">
                                    <i class="fa fa-video mr-2"></i> {{ $course->videos->count() }}
                                </span>
                            </h3>
                            <div class="row mt-4 text-center">
                                @forelse ($course->videos as $video)
                                    <div class="col-md-3 col-sm-4 video-thumb-content">
                                        <a href="{{ route('ogrenci.videos.show', ['course' => $course, 'video' => $video]) }}">
                                            <img src="{{ asset('storage/' . $course->files->first()->path) }}"
                                                 class="img-thumbnail"/>

                                            <span class="video-thumb-text text-dark d-block">
                                            {{ $video->title }}
                                        </span>
                                        </a>
                                    </div>
                                @empty
                                    <p></p>
                                @endforelse
                            </div>
                            <div class="row">
                                <div class="col-12 pull-right text-right">
                                    <a href="{{ route('ogrenci.videos.show', ['course' => $course, 'video' => $course->videos()->first()]) }}" class="btn-video-play pull-right">
                                        İzle
                                        <i class="fa fa-play ml-3"></i></a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
@endsection
