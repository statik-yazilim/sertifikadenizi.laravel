@extends('student.app')

@section('content')
    <h1 class="h3 mb-4 text-gray-800">Kurslar</h1>
    <p class="mb-4"></p>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-secondary">{{ $course->name }}</h6>
        </div>
        <div class="card-body">
            <h1 class="text-center h1 mb-3">{{ $video->title }}</h1>
            <hr />
            <div class="row">
                <div class="col-lg-8 col-md-12 box-shadow">
                    <div class="row text-center">
                        <div class="col-12">
                            <?php
                            if (strpos($video->link, 'embed') === false)
                            {
                                parse_str(parse_url($video->link, PHP_URL_QUERY), $link);
                                $link = 'https://youtube.com/embed/' . $link['v'];
                            }
                            else
                                $link = $video->link;
                            ?>
                            <iframe class="education-video"  src="{{ $link }}" width="100%" height="400" frameborder="0"   webkitallowfullscreen mozallowfullscreen
                                    allowfullscreen
                            ></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 box-shadow">
                    <div class="lesson-detail-right-bar box-shadow">
                        <div class="col-md-12 text-center">
                            <h3>Diğer Videolar</h3>
                        </div>
                        @foreach ($course->videos as $video)
                        <a href="{{ route('ogrenci.videos.show', compact('course', 'video')) }}" class="right-video row box-shadow mt-2 mb-2">
                            <div class="col-lg-4">
                                <img id="logo" src="{{ asset('storage/' . $course->files->first()->path) }}" />
                            </div>
                            <div class="col-lg-8 mt-2">
                                {{ $video->title }}
                                <br />
                                @if(auth()->user()->didWatchVideo($video))
                                <span class="badge badge-success"><i class="fa fa-check"></i> İzlendi</span>
                                @else
                                <span class="badge badge-danger"><i class="fa fa-times"></i> İzlenmedi</span>
                                @endif
                            </div>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
