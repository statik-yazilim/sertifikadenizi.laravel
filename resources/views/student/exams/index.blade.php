@extends('student.app')
@section('content')
    <h1 class="h3 mb-4 text-gray-800">Sınavlar</h1>
    <p class="mb-4"></p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-secondary">Sınavlar</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Sınav</th>
                        <th>Başlangıç Tarihi</th>
                        <th>Son Katılım Tarihi</th>
                        <th>Geçer Yüzdesi</th>
                        <th>Soru Sayısı</th>
                        <th>Sınav Süresi</th>
                        <th>Doğru/Yanlış</th>
                        <th>Sınav Sonucu</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($exams as $exam)
                        <tr>
                            <td>{{ $exam->name }}</td>
                            <td>{{ \Carbon\Carbon::parse($exam->begin_date)->format('d/m/Y') }}</td>
                            <td>{{ \Carbon\Carbon::parse($exam->end_date)->format('d/m/Y') }}</td>
                            <td>% {{ $exam->percentage }}</td>
                            <td>{{ $exam->questions->count() }}</td>
                            <td>{{ $exam->duration }} dk</td>
                            @if ($exam->instance)
                                @if ($exam->instance->currentUserResult()->isNotEmpty())
                                <td><b class="text-success">{{ $exam->instance->currentUserResult()->first()->true_answer_count }} D</b> /
                                    <b class="text-danger">{{ $exam->questions->count() - $exam->instance->currentUserResult()->first()->true_answer_count
                                 }} Y</b> (% {{ $exam->instance->currentUserResult()->first()->success_rate }})</td>
                                <td>
                                    @if ($exam->instance->currentUserResult()->first()->is_success)
                                        <span class="badge badge bg-success text-white p-1"><i class="fa fa-check"></i> Sınavı Geçtiniz</span>
                                    @else
                                        <span class="badge badge bg-danger text-white p-1"><i class="fa fa-remove"></i> Sınavdan Kaldınız</span>
                                    @endif
                                    <br />
                                    <a class="btn btn-sm btn-info p-1 text-white" href="{{ route('ogrenci.exams.show', $exam->instance->id)  }}">
                                        <i class="fa fa-eye"></i> Sınav Sonucunu görüntüle</a>
                                </td>
                                <td>
                                    <span class="badge badge-success p-1">
                                        <i class="fa fa-check mr-2"></i>
                                        {{ $exam->instance->currentUserResult()->first()->created_at }} tarihinde Katıldınız
                                    </span>
                                </td>
                                @else
                                <td colspan="2">Sınava Katılım Bekleniyor</td>
                                <td>
                                    <a class="btn btn-sm btn-info p-1 text-white" href="{{ route('ogrenci.exams.join', $exam) }}"> <i class="fa fa-eye"></i> Sınava Katıl</a>
                                </td>
                                @endif
                            @else
                                <td colspan="2">Sınava Katılım Bekleniyor</td>
                                <td>
                                    <a class="btn btn-sm btn-info p-1 text-white" href="{{ route('ogrenci.exams.join', $exam) }}"> <i class="fa fa-eye"></i> Sınava Katıl</a>
                                </td>
                            @endif
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9">Sınav bulunmuyor.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
