@extends('student.app')
<?php /** @var \App\Models\ExamResult[] $examResults **/ ?>
@section('content')
    <h1 class="h3 mb-4 text-gray-800">{{ $examInstance->exam->name }} - Sınav Sonuçları</h1>
    <p class="mb-4"></p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-secondary">Sorular Cevaplar</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Soru</th>
                            <th scope="col">İşaretlenen Cevap</th>
                            <th scope="col">Doğru Cevap</th>
                            <th scope="col">Sonuç</th>
                        </tr>
                    </thead>
                    <tbody>

                @foreach ($examResults->answers as $i => $answer)
                        <tr>
                            <th scope="row">{{ $i+1 }}</th>
                            <td>{{ $answer->question->question_text }}</td>
                            @if (!empty($answer->answer->answer_text))
                            <td>{{ $answer->answer->answer_text }}</td>
                            @else
                            <td>(Boş)</td>
                            @endif
                            <td>{{ $answer->trueAnswer->answer_text }}</td>
                            <td style="background-color: {{ $answer->is_true ? "green" : "red" }}; color:white;">
                                {{ $answer->is_true ? "Doğru" : "Yanlış" }}
                            </td>
                        </tr>
                @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
