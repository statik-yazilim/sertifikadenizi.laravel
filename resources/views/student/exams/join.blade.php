@extends('student.app')
<?php /* @var \App\Models\StudentExam[] $examInstance */ ?>
@section('content')
    <h1 class="h3 mb-4 text-gray-800" id="examStart" data-id="{{ $examInstance->id }}">{{ $examInstance->exam->name }} <span id="remainingTime"></span></h1>
    <p class="mb-4"></p>

    @foreach ($examInstance->exam->questions as $i => $question)
        <div class="card shadow mb-4">
            <div class="card-header py-3">

            </div>
            <div class="card-body">
                <div class="panel-body mt-3">

                    <div class="row question-answer-area" data-id="{{ $question->id }}">
                        <div class="col-12 question-area">
                            <p>
                                <b class="mr-2 text-main">SORU {{ $i+1 }}: </b>
                                <span>{{ $question->question_text }}</span>
                            </p>
                        </div>
                        <div class="col-12 answer-area">
                            @foreach ($question->answers as $answer)
                                <div class="radio">
                                    <input class="radio-answer" data-id="{{ $answer->id }}" type="radio" name="optionsRadios-{{ $question->id }}"
                                           id="optionsRadios-{{ $question->id }}-{{ $answer->id }}-{{ $i }}">
                                    <label for="optionsRadios-{{ $question->id }}-{{ $answer->id }}-{{ $i }}">
                                        <span>{{ $answer->answer_text }}</span>
                                    </label>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <div class="panel-footer mt-4">
        <a id="examResultBtn" href="#" class="btn btn-success text-white btn-block btn-lg" onclick="CreateResult()" role="button">Sınavı Tamamla</a>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/moment-with-locales.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var examDuration = {{ $examInstance->exam->duration }};

        var datetime = GetStartDate({{ $examInstance->id }});
        var expiryDate = GetExpiryDate({{ $examInstance->id }}); //moment(datetime).add(examDuration, 'm').toDate();

        function timeOutCallBack() {
            Swal.fire({
                title: "Süre Bitti",
                html: "Sınav süresi bitti. Sonucunuz hesaplanıyor.",
                type: "warning",
                confirmButtonText: "Tamam",
            }).then((result) => {
                if (result.value) {
                    CreateResultCallback()
                }
            });
        }

        function GetExpiryDate(id) {

            var key = "ex" + "_" + id;
            var cookieValue = localStorage[key];

            if (IsNullOrUndefined(cookieValue)) {
                var datetime = GetStartDate({{ $examInstance->id }});
                var expiryDate = moment(datetime).add(examDuration, 'm').toDate();
                localStorage[key] = expiryDate;
            }

            return localStorage[key];
        }

        function GetStartDate(id) {
            var key = "et" + "_" + id;
            var cookieValue = localStorage[key];
            console.log(cookieValue);

            if (IsNullOrUndefined(cookieValue)) {
                SetStartDate(id);
                return GetStartDate(id);
            } else {
                return cookieValue;
            }
        }

        function SetStartDate(id) {
            var key = "et" + "_" + id;
            localStorage[key] = new Date();
            console.log(localStorage[key]);
        }

        function startTimer(duration, display) {
            duration = new Date(duration).getTime();
            var timer = duration, minutes, seconds;
            var x = setInterval(function () {
                var now = new Date().getTime(),
                    distance = duration - now;

                minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                seconds = Math.floor((distance % (1000 * 60)) / 1000);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.text(minutes + ":" + seconds);

                if (distance < 0) {
                    clearInterval(x);
                    display.text("Süre Doldu!");
                }
            }, 1000);
        }

        function IsNullOrUndefined(field) {
            return field === undefined || field === "null" || field === null;
        }

        function CreateResult() {
            Swal.fire({
                title: "Sınavı Tamamla",
                html: "Sınavı tamamlama işlemini onaylayın.",
                type: "info",
                showCancelButton: true,
                confirmButtonText: "Tamamla",
                cancelButtonText: "Vazgeç"
            }).then((result) => {
                if (result.value) {
                    try {
                        CreateResultCallback()
                    } catch (err) {
                        console.log("No CallBack")
                    }
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    return;
                }
            });
        }

        function CreateResultCallback() {
            var examInstanceId = $("#examStart").attr("data-id");
            var answeredQuestions = [];
            $(".question-answer-area").each(function (index, question) {
                var questionId = $(question).attr("data-id");
                var answerId = null;

                $(question).children(".answer-area").children(".radio").children(".radio-answer").each(function (index, answer) {
                    var currentAnswerId = $(answer).attr("data-id");
                    var isCheckedAnswer = $(answer).is(":checked");
                    if (isCheckedAnswer) {
                        answerId = currentAnswerId;
                    }
                });
                var answeredQuestion = {
                    examQuestionId: questionId,
                    examCustomerAnswerId: answerId
                };
                answeredQuestions.push(answeredQuestion);
            });

            $("#examResultBtn").attr("disabled", true);
            $("#examResultBtn").text("Cevaplar işleniyor Lütfen Bekleyin...");
            $.ajax({
                type: "post",
                url: '{{ route('ogrenci.exams.finish') }}',
                data: {
                    examInstanceId: examInstanceId,
                    examResultDetail: answeredQuestions
                },
                success: function (result) {
                    $("#examResultBtn").attr("disabled", false);
                    $("#examResultBtn").text("Sınavı Tamamla");
                    if (result.resultType === "ok") {
                        window.location.href = "{{ route('ogrenci.exams.index') }}";
                    } else {
                        Swal.fire({
                            title: result.title,
                            html: result.message,
                            type: "err",
                            confirmButtonText: "Tamam",
                        })
                    }
                }
            });
        }

        window.onload = function () {
            var startDate = GetStartDate({{ $examInstance->id }}),
                display = $("#remainingTime");

            if (IsNullOrUndefined(startDate))
                SetStartDate({{ $examInstance->id }});
            else {

                var expiryDate = moment(startDate).add(examDuration, 'm').toDate(); //date
                var currentDate = moment(new Date());

                if (currentDate >= expiryDate) {
                    timeOutCallBack()
                } else {
                    console.log("continue");
                }
            }

            startTimer(expiryDate, display);
        }
    </script>
@endpush
