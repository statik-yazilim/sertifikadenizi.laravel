@extends('student.app')

@section('content')


    <h1 class="h3 mb-4 text-gray-800">Hocaya Soru Sor</h1>
    <p class="mb-4"></p>
    <form action="{{ route('ogrenci.ask.store') }}" method="post">
        @csrf
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-secondary">Hocaya Soru Sor</h6>
            </div>
            <div class="card-body">
                @if (!empty($thread))
                    @forelse ($thread->personalMessages as $message)
                        @if ($message->user->id === auth()->id())
                            <div class="card shadow mb-4 border-left-primary">
                                <div class="card-header">{{ auth()->user()->name }}:</div>
                                <div class="card-body">
                                    {{ $message->body }}
                                </div>
                            </div>
                        @else
                            <div class="card shadow mb-4 border-left-success ">
                                <div class="card-header">Hoca:</div>
                                <div class="card-body">
                                    {{ $message->body }}
                                </div>
                            </div>
                        @endif
                    @empty
                        Mesaj bulunmuyor.
                    @endforelse
                @else
                    Mesaj bulunmuyor.
                @endif
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <textarea type="text" name="message" class="form-control"></textarea>
                        </div>
                        <div class="col-md-12 mt-4 text-right">
                            <button class="btn btn-success btn-circle"><i class="fas fa-check"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
