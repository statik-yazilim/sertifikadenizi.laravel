<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-secondary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('ogrenci.index') }}">
        <img id="logo" src="{{ asset('img/Sertifika-dunyas.png') }}" alt="logo">
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('ogrenci.index') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Ana Sayfa</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">


    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('ogrenci.courses.index') }}" >
            <i class="fas fa-fw fa-chalkboard"></i>
            <span>Kurslarım</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('ogrenci.exams.index') }}">
            <i class="fas fa-fw fa-university"></i>
            <span>Sınavlarım</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{ route('ogrenci.ask.index') }}">
            <i class="fas fa-fw fa-question"></i>
            <span>Hocaya Soru Sor</span>
        </a>
    </li>
{{--    <li class="nav-item">
        <a class="nav-link collapsed" href="/odemeler" >
            <i class="fas fa-fw fa-credit-card"></i>
            <span>Ödemelerim</span>
        </a>
    </li>--}}


</ul>
<!-- End of Sidebar -->
