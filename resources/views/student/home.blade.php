@extends('student.app')

@section('content')

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Ana Sayfa</h1>

        </div>

        <div class="row">

            <div class="col-lg-4 mb-4">
                <a href="{{ route('ogrenci.courses.index') }}"> <div class="card shadow mb-4">
                        <div class="card-header py-3">
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                                <i class="fa fa-2x fa-graduation-cap text-dark menu-icon-lesson"></i>
                                <span class="d-block text-dark mt-2">Kurslarım</span>
                            </div>
                        </div>
                    </div></a>
            </div>
            <div class="col-lg-4 mb-4">
                <a href="{{ route('ogrenci.exams.index') }}"><div class="card shadow mb-4">
                        <div class="card-header py-3">
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                                <i class="fa  fa-2x fa-university text-dark cursor-pointer"></i>
                                <span class="d-block text-dark mt-2">Sınavlarım</span>
                            </div>
                        </div>
                    </div></a>
            </div>
            <div class="col-lg-4 mb-4">
                <a href="{{ route('ogrenci.ask.index') }}"><div class="card shadow mb-4">
                        <div class="card-header py-3">
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                                <i class="fa  fa-2x fa-question text-dark cursor-pointer"></i>
                                <span class="d-block text-dark mt-2">Hocaya Soru Sor</span>
                            </div>
                        </div>
                    </div></a>
            </div>

            <div class="col-lg-12 mb-4">

                <!-- Öğrenci Bilgileri -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-secondary">Öğrenci Bilgileri</h6>
                    </div>
                    <div class="card-body">
                        <div class="text-left">
                            <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 10rem;"
                                 src="{{ asset('img/undraw_profile.svg') }}" alt="">
                        </div>
                        <table class="table table-no-border mt-2">
                            <tbody>
                            <tr>
                                <td>Ad</td>
                                <td>:</td>
                                <td>{{ auth()->user()->name }}</td>
                            </tr>
                            <tr>
                                <td>E-posta</td>
                                <td>:</td>
                                <td>{{ auth()->user()->email }}</td>
                            </tr>
                            <tr>
                                <td>Akademi</td>
                                <td>:</td>
                                <td>Sertifika Denizi</td>
                            </tr>

                            <tr>
                                <td>T.C No</td>
                                <td>:</td>
                                <td>{{ auth()->user()->tc_no }}</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- Öğrenci Bilgileri -->
            </div>
        </div>
@endsection
