@extends('admin.app')

@section('content')
    <div class="right-side">
        <section class="dashboard-content">
            <div class="panel-heading">
                <h2><img src="/admin/assets/icons/dashboard.png" alt=""> Yönetim Paneli</h2>
            </div>
            <div class="black-bg wellcome-text">
                <h3>Statik Yazılım Yönetim Paneline Hoş Geldiniz.</h3>
            </div>
        </section>
    </div>
@endsection
