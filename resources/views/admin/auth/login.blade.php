@extends('admin.app')

@section('content')
<div class="right-side">
    <section class="dashboard-content">
        <div class="panel-heading">
            <h2><i class="fas fa-sign-in-alt"></i> Giriş Yap</h2>
        </div>
        <div class="login black-bg">
            <div class="form-one">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('admin.login') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="email">Kullanıcı</label>
                        <input type="text" name="email" id="email" value="{{ old('name') }}" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="password">Şifre</label>
                        <input type="password" name="password" id="password" required autocomplete="current-password">
                    </div>
                    <input type="submit" name="submit" class="btn-one m-0" value="GİRİŞ" class="btn-giris"><br/>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection
