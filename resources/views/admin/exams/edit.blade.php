@extends('admin.app')

@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Sınav Düzenle</h2>
            </div>
            <div class="user-add black-bg">
                <div class="form-one">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.exams.update', $exam) }}" method="post">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Başlık</label>
                                    <input type="text" name="name" id="name" class="style-one" value="{{ $exam->name }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="duration">Sınav Süresi (Dakika)</label>
                                    <input type="number" name="duration" id="duration" value="{{ $exam->duration }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="percentage">Sınav Geçme Oranı (Yüzde)</label>
                                    <input type="number" name="percentage" id="percentage" value="{{ $exam->percentage }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="begin_date">Sınav Başlangıç Tarihi</label>
                                    <input type="date" name="begin_date" id="begin_date" value="{{ $exam->begin_date }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="end_date">Son Katılım Tarihi</label>
                                    <input type="date" name="end_date" id="end_date" value="{{ $exam->end_date }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="status">Sınav Açılsın Mı?</label>
                                    <input type="checkbox" name="status" id="status"{{ $exam->status ? ' checked' : '' }}>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="course_id">Kurs</label>
                                    <select class="style-one" id="course_id" name="course_id">
                                        @foreach($courses as $course)
                                            <option value="{{ $course->id }}"{{ $exam->course->id == $course->id ? ' selected' : '' }}>{{ $course->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="duzenle">
                                <input type="submit" value="Düzenle" class="btn-one">
                            </div>
                        </div>
                        <input type="hidden" name="type" value="exam">
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
