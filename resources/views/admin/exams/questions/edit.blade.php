@extends('admin.app')

@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Soru Düzenle - {{ $question->question_text }}</h2>
            </div>
            <div class="user-add black-bg">
                <div class="form-one">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.exams.questions.update', compact('exam', 'question')) }}" method="post">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="question_text">Soru</label>
                                    <input type="text" name="question_text" id="question_text" class="style-one" value="{{ $question->question_text }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-10"><label>Cevaplar</label></div>
                                        <div class="col-md-2"><label>Doğru Cevap</label></div>
                                    </div>
                                    @foreach ($question->answers as $answer)
                                    <div class="row mt-2">
                                        <div class="col-md-10"><input type="text" name="answers[{{ $answer->id }}]" value="{{ $answer->answer_text }}" class="style-one"></div>
                                        <div class="col-md-2"><input type="radio" name="trueAnswer" value="{{ $answer->id }}" {{ $answer->is_true ? ' checked' : '' }}></div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="duzenle">
                                <input type="submit" value="Düzenle" class="btn-one">
                            </div>
                        </div>
                        <input type="hidden" name="type" value="announcement">
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
