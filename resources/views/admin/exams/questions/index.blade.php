@extends('admin.app')
<?php /* @var \App\Models\Admin\Exam $exam */ ?>
<?php /* @var \App\Models\Admin\ExamQuestion[] $questions */ ?>
@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Sınavlar - <a href="{{ route('admin.exams.index') }}">{{ $exam->name }}</a> </h2>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="user-add black-bg">
                <div class="form-one">
                    <div class="section-heading">
                        <a data-toggle="collapse" href="#collapseAddForm" role="button" aria-expanded="{{ $errors->any() ? 'true' : 'false' }}" aria-controls="collapseAddForm">
                            Soru Ekle
                        </a>
                    </div>
                    <form action="{{ route('admin.exams.questions.store', $exam) }}" method="post" enctype="multipart/form-data" id="collapseAddForm"
                          class="collapse {{ $errors->any() ? ' show' : ''}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="question_text">Soru</label>
                                    <input type="text" name="question_text" id="question_text" class="style-one" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-10"><label>Cevaplar</label></div>
                                        <div class="col-md-2"><label>Doğru Cevap</label></div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-10"><input type="text" name="answers[]" class="style-one"></div>
                                        <div class="col-md-2"><input type="radio" name="trueAnswer[0]"></div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-10"><input type="text" name="answers[]" class="style-one"></div>
                                        <div class="col-md-2"><input type="radio" name="trueAnswer[1]"></div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-10"><input type="text" name="answers[]" class="style-one"></div>
                                        <div class="col-md-2"><input type="radio" name="trueAnswer[2]"></div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-10"><input type="text" name="answers[]" class="style-one"></div>
                                        <div class="col-md-2"><input type="radio" name="trueAnswer[3]"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="ekle">
                                <input type="submit" value="Ekle" class="btn-one">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="user-add black-bg">
                <div class="form-one">
                    <div class="section-heading">
                        <form method="post" action="{{ route('admin.exams.question.copy', $exam) }}">
                            @csrf
                            <select name="from">
                                @foreach ($exams as $exam2)
                                    <option value="{{ $exam2->id }}">{{ $exam2->name }}</option>
                                @endforeach
                            </select>
                            <input type="submit" class="btn-one" value="Soruları Kopyala">
                        </form>
                    </div>
                </div>
            </div>
            <div class="table-one black-bg">
                <div class="section-heading">
                    Soru Tablosu
                </div>
                <table class="datatable table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Soru</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($exam->questions as $i => $question)
                        <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{ $question->question_text }}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.exams.questions.edit', ['exam' => $exam, 'question' => $question]) }}" class="btn-one edit-btn">Düzenle</a>
                                <a href="javascript:void(0)" class="btn-one delete-btn" onclick="deleteSubItem(this)"
                                   data-url="{{ route('admin.exams.questions.destroy', ['exam' => $exam, 'question' => $question]) }}">Sil</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
