@extends('admin.app')
<?php /* @var \App\Models\Admin\Exam[] $exam */ ?>
@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Sınav Sonuçları - {{ $exam->name }}</h2>
            </div>
            <div class="table-one black-bg">
                <div class="section-heading">
                    Sınav Sonuçları
                </div>
                <table class="datatable table table-striped table-bordered" id="exam-datatable">
                    <thead>
                    <tr>
                        <th>Kullanıcı</th>
                        <th>TC No</th>
                        <th>Katılım Tarihi</th>
                        <th>IP Adresi</th>
                        <th>Sınav Sonucu</th>
                        <th>Doğru / Yanlış</th>
{{--                        <th>İşlemler</th>--}}
                    </tr>
                    </thead>
                    <tbody>
            @if (!empty($exam->instance))
                @forelse ($exam->instance->results as $result)
                        <tr>
                            <td>{{ $result->user->name }}</td>
                            <td>{{ $result->user->tc_no }}</td>
                            <td>{{ $result->created_at->format('d.m.Y') }}</td>
                            <td>{{ $result->ip_address }}</td>
                            <td>{{ $result->is_success ? 'Geçti' : 'Kaldı' }}</td>
                            <td><b class="text-success">{{ $result->true_answer_count }} D</b> /
                                <b class="text-danger">{{ $exam->questions->count() - $result->true_answer_count
                                 }} Y</b> (% {{ $result->success_rate }})</td>
                            {{--<td class="text-right">
                                <a href="javascript:void(0)" class="btn-one delete-btn" onclick="deleteSubItem(this)"
                                   data-url="{{ route('admin.exams.destroy', $exam) }}">Sil</a>
                            </td>--}}
                        </tr>
                    @empty
                @endforelse
            @else
                <tr>
                    <td colspan="6">Sınav öğrencileri eklenmemiş.</td>
                </tr>
            @endif
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            datatable.destroy();
            datatable = $('#exam-datatable').DataTable({
                language: datatableLanguage,
                destroy: true,
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [0,1,2,3,4,5]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [0,1,2,3,4,5]
                        },
                        orientation: 'landscape',
                        pageSize: 'A4',
                        alignment: 'center',
                        customize: function(doc) {
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    }
                ]
            });
        });
    </script>
@endpush
