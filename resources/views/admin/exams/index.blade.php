@extends('admin.app')
<?php /* @var \App\Models\Admin\Exam[] $exams */ ?>
@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Sınavlar</h2>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="user-add black-bg">
                <div class="form-one">
                    <div class="section-heading">
                        <a data-toggle="collapse" href="#collapseAddForm" role="button" aria-expanded="{{ $errors->any() ? 'true' : 'false' }}" aria-controls="collapseAddForm">
                            Sınav Ekle
                        </a>
                    </div>
                    <form action="{{ route('admin.exams.store') }}" method="post" enctype="multipart/form-data" id="collapseAddForm"
                          class="collapse{{ $errors->any() ? ' show' : ''}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">Başlık</label>
                                    <input type="text" name="name" id="name" class="style-one" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="duration">Sınav Süresi (Dakika)</label>
                                    <input type="number" name="duration" id="duration" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="percentage">Sınav Geçme Oranı (Yüzde)</label>
                                    <input type="number" name="percentage" id="percentage" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="begin_date">Sınav Başlangıç Tarihi</label>
                                    <input type="date" name="begin_date" id="begin_date" class="style-one" value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="end_date">Son Katılım Tarihi</label>
                                    <input type="date" name="end_date" id="end_date" class="style-one" value="{{ \Carbon\Carbon::now()->addDays(1)->format('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="status">Sınav Açılsın Mı?</label>
                                    <input type="checkbox" name="status" id="status">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="course_id">Kurs</label>
                                    <select class="style-one" id="course_id" name="course_id">
                                        @foreach($courses as $course)
                                            <option value="{{ $course->id }}">{{ $course->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="ekle">
                                <input type="submit" value="Ekle" class="btn-one">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-one black-bg">
                <div class="section-heading">
                    Sınav Tablosu
                </div>
                <table class="datatable table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Başlık</th>
                        <th>Kurs</th>
                        <th>Başlangıç Tarihi</th>
                        <th>Durum</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($exams as $exam)
                        <tr>
                            <td>{{ $exam->name }}</td>
                            <td>{{ $exam->course->name }}</td>
                            <td>{{ \Carbon\Carbon::parse($exam->begin_date)->format('d/m/Y') }}</td>
                            <td>{{ $exam->status_text }}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.exams.instance.create', ['exam' => $exam]) }}" class="btn-one edit-btn">
                                    Katılacak  Kişiler
                                </a>
                                <a href="{{ route('admin.exams.show', $exam) }}" class="btn-one accept-btn">Sınav Sonuçları</a>
                                <a href="{{ route('admin.exams.questions.index', $exam) }}" class="btn-one accept-btn">Sınav Soruları</a>
                                <a href="{{ route('admin.exams.edit', $exam) }}" class="btn-one edit-btn">Düzenle</a>
                                <a href="javascript:void(0)" class="btn-one delete-btn" onclick="deleteSubItem(this)"
                                   data-url="{{ route('admin.exams.destroy', $exam) }}">Sil</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
