@extends('admin.app')
@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Sınavlar</h2>
            </div>
            <div class="table-one black-bg">
                <form action="{{ route('admin.exams.instance.store', ['exam' => $exam]) }}" method="post">
                    @csrf
                    <div class="section-heading">Katılacak Kişileri Seçiniz</div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Üye</th>
                            <th>Son Sınav Sonucu</th>
                            <th><input type="checkbox" id="select-all"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{!! $user->didUserPass() ? '<b class="text-success">Geçti</b>' : '' !!} </td>
                                <td>
                                @if (!empty($exam->instance) && !empty($user->didJoinExam($exam)))
                                    <input type="checkbox" name="joining[{{ $user->id }}]" checked="checked">
                                @else
                                <input type="checkbox" name="joining[{{ $user->id }}]">
                                @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <input type="submit" value="Ekle" class="btn-one">
                    @if (!empty($exam->instance))
                    <input type="hidden" name="instance_id" value="{{ $exam->instance->id }}">
                    @endif
                </form>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $('#select-all').click(function (e) {
            var checked = this.checked;

            $(':checkbox').each(function () {
                this.checked = checked;
            });
        })
    </script>
@endpush
