@extends('admin.app')

@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Kullanıcı Düzenle</h2>
            </div>
            <div class="user-add black-bg">
                <div class="form-one">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.users.update', $user) }}" method="post">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">İsim</label>
                                    <input type="text" name="name" id="name"
                                           value="{{ $user->name }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tc_no">TC No</label>
                                    <input type="number" name="tc_no" id="tc_no"
                                           value="{{ $user->tc_no }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">E-posta</label>
                                    <input type="email" name="email" id="email"
                                           value="{{ $user->email }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password">Şifre</label>
                                    <input type="password" name="password" id="password"
                                           value="" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="role">Rol</label>
                                    <select name="role" id="role"
                                            class="style-one">
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}"{{ $role->id === $user->roles()->first()->id ? ' selected' : '' }}>{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="duzenle">
                                <input type="submit" value="Düzenle" class="btn-one">
                            </div>
                        </div>
                        <input type="hidden" name="type" value="announcement">
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
