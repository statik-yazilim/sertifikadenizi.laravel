@extends('admin.app')
<?php /* @var \App\Models\User[] $users */ ?>
@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Kullanıcılar</h2>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="user-add black-bg">
                <div class="form-one">
                    <div class="section-heading">
                        <a data-toggle="collapse" href="#collapseAddForm" role="button" aria-expanded="{{ $errors->any() ? 'true' : 'false' }}" aria-controls="collapseAddForm">
                            Kullanıcı Ekle
                        </a>
                    </div>
                    <form action="{{ route('admin.users.store') }}" method="post" enctype="multipart/form-data" id="collapseAddForm"
                          class="collapse{{ $errors->any() ? ' show' : ''}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">İsim</label>
                                    <input type="text" name="name" id="name"
                                           class="style-one"
                                    value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tc_no">TC No</label>
                                    <input type="number" name="tc_no" id="tc_no"
                                           class="style-one"
                                           value="{{ old('tc_no') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">E-posta</label>
                                    <input type="email" name="email" id="email"
                                           class="style-one"
                                           value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password">Şifre</label>
                                    <input type="password" name="password" id="password"
                                           value="" class="style-one"
                                           value="{{ old('password') }}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="role">Rol</label>
                                    <select name="role" id="role"
                                            class="style-one">
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="ekle">
                                <input type="submit" value="Ekle" class="btn-one">
                            </div>
                        </div>
                        <input type="hidden" name="type" value="announcement">
                    </form>
                </div>
            </div>
            <div class="table-one black-bg">
                <div class="section-heading">
                    Kurs Tablosu
                </div>
                <table class="datatable table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>İsim</th>
                        <th>E-Posta</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.users.edit', $user) }}" class="btn-one edit-btn">Düzenle</a>
                                <a href="javascript:void(0)" class="btn-one delete-btn" onclick="deleteSubItem(this)"
                                   data-url="{{ route('admin.users.destroy', $user) }}">Sil</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
