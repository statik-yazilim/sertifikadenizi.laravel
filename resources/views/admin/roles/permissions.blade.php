@extends('admin.app')

@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-10"><h2><i class="fas fa-sign-in-alt"></i>Yetki Düzenle</h2>
                    </div>
                    <div class="col-md-2 text-right">
                        <button class="btn-one" form="permissionForm">KAYDET</button>
                    </div>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{ route('admin.roles.permissions.store', $role) }}" method="post" id="permissionForm">
                @csrf

                <div class="row">
                    @foreach ($permission_categories as $permission_category)

                        <div class="col-md-4">
                            <div class="user-add black-bg">
                                <div class="form-one">
                                    <div class="section-heading">
                                        {{ $permission_category->name }}
                                    </div>

                                    <div class="kt-widget2">
                                        @foreach($permission_category->permissions as $perm)
                                            <?php
                                            $per_found = null;

                                            if (isset($role))
                                            {
                                                $per_found = $role->hasPermissionTo($perm->name);
                                            }
                                            ?>

                                            <div class="form-group">
                                                <label>{{ $perm->description ?? $perm->name }}</label>
                                                <input data-category-id="{{ $permission_category->id }}" type="checkbox" name="permissions[]"
                                                       value="{{ $perm->name }}"{{ $per_found ? ' checked' : '' }}>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </form>
        </section>
    </div>
@endsection
