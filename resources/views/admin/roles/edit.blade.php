@extends('admin.app')

@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Rol Düzenle</h2>
            </div>
            <div class="user-add black-bg">
                <div class="form-one">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.roles.update', $role) }}" method="post">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Başlık</label>
                                    <input type="text" name="name" id="name"
                                           value="{{ $role->name }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="description">Açıklama</label>
                                    <input type="text" name="description" id="description"
                                           value="{{ $role->description }}" class="style-one">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="duzenle">
                                <input type="submit" value="Düzenle" class="btn-one">
                            </div>
                        </div>
                        <input type="hidden" name="type" value="role">
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
