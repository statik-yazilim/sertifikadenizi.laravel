<div class="modal fade" id="imageCropModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" style="max-width: 90%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Resim Düzenle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="cropper-container">
                    <img id="image-cropper">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                <button type="button" class="btn btn-primary" id="cropButton">Düzenle</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        var $modal = $("#imageCropModal"),
            $cropper = $("#image-cropper"),
            $container = $(".cropper-container"),
            $uploadedImage, maxHeight, maxWidth, imageInput, model, modelId, attr, thumb;

        $(".toBeCropped").on("change", function (e) {
            var input = this;

            imageInput = $(this);
            maxHeight = imageInput.data('height');
            maxWidth = imageInput.data('width');
            model = imageInput.data('model');
            modelId= imageInput.data('model-id');
            attr = imageInput.data('attr');
            thumb = $("#" + attr + "InputThumb");


            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.readAsDataURL(input.files[0]);

                reader.onload = function (e) {
                    $uploadedImage = this.result;
                    $container.width(maxWidth).height(maxHeight);

                    $modal.modal("show");
                }
            }
        });

        $modal.on("shown.bs.modal", function (e) {
            $cropper.attr("src", $uploadedImage);

            $cropper.cropper({
                viewMode: 3,
                guides: false,
                rotatable: false,
                scalable: false,
                autoCropArea: 1,
                cropBoxResizable: false,
                toggleDragModeOnDblclick: false,
                dragMode: 'move',

                /*data: {
                    width: (maxWidth),
                    height: (maxHeight),
                },*/
            });
        }).on("hidden.bs.modal", function () {
            $cropper.cropper('destroy');
        });

        $("#cropButton").on("click", function () {
            var canvas;

            if ($cropper) {
                canvas = $cropper.data('cropper').getCroppedCanvas();
                if (imageInput) {
                    console.log(canvas);
                    thumb.attr("src", canvas.toDataURL());
                }

                canvas.toBlob(function (blob) {
                    var formData = new FormData(),
                        name = imageInput.val().split("\\\\").pop();

                    formData.append(attr, blob, name);
                    formData.append("attr", attr);
                    formData.append("model", model);
                    formData.append("modelId", modelId);

                    $.ajax("{{ route('admin.file.upload') }}", {
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,

                        success: function (e) {
                            console.log(e);

                            $("#" + imageInput.attr("id") + "Hidden").val(e.result.id);
                            $modal.modal('hide');
                        }
                    });
                });
            }
        });
    </script>
@endpush
