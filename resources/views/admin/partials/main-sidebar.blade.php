<div class="left-side">
    <div class="main-sidebar">
        <div class="top">
            <div class="logo">
                <img  src="{{ asset('img/Sertifika-dunyas.png') }}" alt="">
            </div>
        </div>
        <div class="main-menu">
            <ul>
                <x-admin-menu-item :url="route('admin.index')" title="Ana Sayfa" :icon="asset('admin_assets/assets/icons/dashboard.png')"></x-admin-menu-item>

                @if (Auth::check())
                    <x-admin-menu-item :url="route('admin.courses.index')" title="Kurslar" :icon="asset('admin_assets/assets/icons/group.png')"></x-admin-menu-item>
                    <x-admin-menu-item :url="route('admin.videos.index')" title="Videolar" :icon="asset('admin_assets/assets/icons/media.png')"></x-admin-menu-item>
                    <x-admin-menu-item :url="route('admin.exams.index')" title="Sınavlar" :icon="asset('admin_assets/assets/icons/mortarboard.png')"></x-admin-menu-item>
                    <x-admin-menu-item :url="route('admin.users.index')" title="Kullanıcılar" :icon="asset('admin_assets/assets/icons/user.png')"></x-admin-menu-item>
                    <x-admin-menu-item :url="route('admin.messages.index')" title="Mesajlar" :icon="asset('admin_assets/assets/icons/user.png')"></x-admin-menu-item>


{{--                    <x-admin-menu-item :url="route('admin.roles.index')" title="Yetkiler" :icon="asset('admin_assets/assets/icons/shield.png')"></x-admin-menu-item>--}}

                    <li>
                        <a href="{{ route('admin.logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <img src="{{ asset('admin_assets/assets/icons/logout.png') }}" alt=""><span>Çıkış Yap</span>
                            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
        <div class="bottom">
            <div class="logo">
                <a href="https://statikyazilim.com.tr/">
                    <img src="{{ asset('admin_assets/assets/media/sy-yonetim-paneli.png') }}" alt="">
                </a>
            </div>
        </div>
    </div>
</div>
