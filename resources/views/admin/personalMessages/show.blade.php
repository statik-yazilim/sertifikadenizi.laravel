@extends('admin.app')

@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Öğrenci Mesajları</h2>
            </div>
            <form action="{{ route('admin.messages.store', $thread) }}" method="post">
                @csrf
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-secondary">Hocaya Soru Sor</h6>
                    </div>
                    <div class="card-body">
                        @forelse ($thread->personalMessages as $message)
                            @if ($message->user->id !== auth()->id())
                                <div class="card shadow mb-4 border-left-primary">
                                    <div class="card-header">{{ $thread->sender->name }}:</div>
                                    <div class="card-body">
                                        {{ $message->body }}
                                    </div>
                                </div>
                            @else
                                <div class="card shadow mb-4 border-left-success ">
                                    <div class="card-header">Hoca:</div>
                                    <div class="card-body">
                                        {{ $message->body }}
                                    </div>
                                </div>
                            @endif
                        @empty
                            Mesaj bulunmuyor.
                        @endforelse

                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-11">
                                    <textarea type="text" name="message" class="form-control"></textarea>
                                </div>
                                <div class="col-md-1 m-auto">
                                    <button class="btn btn-success btn-circle"><i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="thread_id" value="{{ $thread->id }}">
            </form>
        </section>
    </div>
@endsection
