@extends('admin.app')
<?php /* @var \App\Models\Admin\PersonalMessage[] $personalMessages */ ?>
@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Öğrenci Soruları</h2>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="table-one black-bg">
                <div class="section-heading">
                    Soru Tablosu
                </div>
                <table class="datatable table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Gönderen</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($threads as $thread)
                        <tr>
                            <td>{{ $thread->sender->name }}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.messages.show', $thread) }}" class="btn-one accept-btn">Görüntüle</a>
                                <a href="javascript:void(0)" class="btn-one delete-btn" onclick="deleteSubItem(this)"
                                   data-url="{{ route('admin.messages.destroy', $thread) }}">Sil</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
