@extends('admin.app')
<?php /* @var \App\Models\Admin\Video[] $videos */ ?>
@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Kurs Videoları</h2>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="user-add black-bg">
                <div class="form-one">
                    <div class="section-heading">
                        <a data-toggle="collapse" href="#collapseAddForm" role="button" aria-expanded="{{ $errors->any() ? 'true' : 'false' }}" aria-controls="collapseAddForm">
                            Video Ekle
                        </a>
                    </div>
                    <form action="{{ route('admin.videos.store') }}" method="post" enctype="multipart/form-data" id="collapseAddForm"
                          class="collapse{{ $errors->any() ? ' show' : ''}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Başlık</label>
                                    <input type="text" name="title" id="title" class="style-one" value="{{ old('name') }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="link">Video Linki</label>
                                    <input type="text" name="link" id="link" class="style-one" value="{{ old('link') }}" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="course_id">Kurs</label>
                                    <select class="style-one" id="course_id" name="course_id">
                                        @foreach ($courses as $course)
                                            <option value="{{ $course->id }}">{{ $course->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="ekle">
                                <input type="submit" value="Ekle" class="btn-one">
                            </div>
                        </div>
                        <input type="hidden" name="type" value="announcement">
                    </form>
                </div>
            </div>
            <div class="table-one black-bg">
                <div class="section-heading">
                    Video Tablosu
                </div>
                <table class="datatable table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Başlık</th>
                        <th>Kurs</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($videos as $video)
                        <tr>
                            <td>{{ $video->title }}</td>
                            <td>{{ $video->course->name }}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.videos.edit', $video) }}" class="btn-one edit-btn">Düzenle</a>
                                <a href="javascript:void(0)" class="btn-one delete-btn" onclick="deleteSubItem(this)"
                                   data-url="{{ route('admin.videos.destroy', $video) }}">Sil</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
