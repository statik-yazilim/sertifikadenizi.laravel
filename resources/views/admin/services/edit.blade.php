@extends('admin.app')

@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Hizmet Düzenle</h2>
            </div>
            <div class="user-add black-bg">
                <div class="form-one">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.services.update', $service) }}" method="post">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Başlık</label>
                                    <input type="text" name="name" id="name"
                                           value="{{ $service->name }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="slug">SEO URL</label>
                                    <input type="text" name="slug" id="slug"
                                           value="{{ $service->slug }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">İçerik</label>
                                    <textarea name="description" id="description" class="summernote">{{ $service->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="duzenle">
                                <input type="submit" value="Düzenle" class="btn-one">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
