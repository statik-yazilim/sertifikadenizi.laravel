<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Yönetim Paneli - {{ config('app.name') }}</title>
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/lib/bootstrap/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/lib/font-awesome/css/all.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/assets/lib/datatables/datatables.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/lib/jquery-ui/jquery-ui.theme.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/lib/summernote/summernote-bs4.min.css') }}"/>
    {{--    <link rel="stylesheet" href="{{ asset('admin_assets/assets/lib/croppie/croppie.css') }}" />--}}
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/lib/cropperjs/cropper.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/lib/lightbox/lightbox.css') }}"/>
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/css/admin.css?v=2') }}"/>

    <script>
        var appUrl = '{{ route('admin.index') }}'
    </script>
    @stack('styles')
</head>
<body class="admin-panel">
<div class="container-fluid">
    <div class="row">
        @include('admin.partials.main-sidebar')
        @yield('content')
    </div>
</div>

@include('admin.partials.cropper')

<script src="{{ asset('admin_assets/assets/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('admin_assets/assets/lib/popperjs/popper.min.js') }}"></script>
<script src="{{ asset('admin_assets/assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/assets/lib/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/assets/lib/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('admin_assets/assets/lib/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('admin_assets/assets/lib/summernote/summernote-tr-TR.min.js') }}"></script>
{{--<script src="{{ asset('admin_assets/assets/lib/croppie/croppie.min.js') }}" />--}}
<script src="{{ asset('admin_assets/assets/lib/cropperjs/cropper.min.js') }}"></script>
<script src="{{ asset('admin_assets/assets/lib/cropperjs/jquery-cropper.min.js') }}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script src="{{ asset('admin_assets/assets/js/admin.js?v=2') }}"></script>

@stack('scripts')
</body>
</html>
