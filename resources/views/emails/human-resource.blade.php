<?php /* @var App\Models\Mails\HumanResource $humanResource */ ?>
<p><b>İsim</b>: {{ $humanResource->name }}</p>
<p><b>Meslek</b>: {{ $humanResource->occupation }}</p>
<p><b>Email</b>: {{ $humanResource->email }}</p>
<p><b>Telefon</b>: {{ $humanResource->phone }}</p>
<p><b>Üniversite</b>: {{ $humanResource->university }}</p>
<p><b>Mesleki Tecrübe</b>: {{ $humanResource->experience }}</p>
<p><b>Yabancı Diller</b>: {{ $humanResource->foreign_languages }}</p>
<p><b>Ehliyet</b>: {{ $humanResource->license }}</p>
<p><b>Mesaj</b>: {{ $humanResource->message }}</p>
@if (!empty($humanResource->cv))
    <p>CV ektedir.</p>
@endif
