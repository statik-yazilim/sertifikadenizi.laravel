<li>
    <a href="{{ $url }}">
        <img src="{{ $icon }}" alt="">
        <span>{{ $title }}</span>
    </a>
</li>
