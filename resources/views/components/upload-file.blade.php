
    <div class="col-md-3">
        <div class="form-group">

            <img id="{{ $id }}InputThumb" src="@if (!empty($selectedIds)) {{ asset('storage/' . $files->path) }} @endif">
        </div>
    </div>

<div class="col-md-9">
    <div class="form-group">
        <label for="imageInput">Resim</label>
        <input type="file" name="{{ $id }}" id="{{ $id }}Input" class="style-one toBeCropped"
               data-width="{{ $width }}" data-height="{{ $height }}"
               data-model-id="{{ $modelId }}"
               data-model="{{ $model }}"
               data-attr="{{ $attr }}">
        <input type="hidden" name="{{ $id }}Ids" id="{{ $id }}InputHidden" value="{{ $selectedIds }}">
    </div>
</div>
