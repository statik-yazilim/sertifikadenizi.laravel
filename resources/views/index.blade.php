<!DOCTYPE html>
<html lang="tr">


<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>

    <!-- Metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="keywords" content="sertifika denizi"/>
    <meta name="description" content=""/>
    <meta name="author" content="https://statikyazilim.com.tr"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="yandex-verification" content="2741e59102484b7e" />

    <!-- Title  -->
    <title>Sertifika Denizi</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.png"/>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,600,700&amp;display=swap" rel="stylesheet">

    <!-- Plugins -->
    <link rel="stylesheet" href="css/plugins.css"/>

    <!-- Core Style Css -->
    <link rel="stylesheet" href="css/style.css"/>

</head>

<body>
<button class="meb-button">
    <img src="/img/meb_logo.jpg" alt="">
</button>
<!-- =====================================
    ==== Start Loading -->

<div class="loading">
    <div class="gooey">
        <span class="dot"></span>
        <div class="dots">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</div>

<!-- End Loading ====
    ======================================= -->


<!-- =====================================
    ==== Start Navbar -->

<nav class="navbar navbar-expand-lg nav-box radius">
    <div class="container">

        <!-- Logo -->
        <a class="logo" href="#">
            <img src="img/Sertifika-dunyas1.png" alt="logo">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"><i class="fas fa-bars"></i></span>
        </button>

        <!-- navbar links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="#" data-scroll-nav="0">Ana Sayfa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="1">Hakkımızda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="2">Eğitici Eğitimi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="3">Eğitmenimiz</a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="4">Ücretlendirme</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="5">Blog</a>
                </li>-->
                <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="6">İletişim</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('ogrenci.index') }}" class="butn butn-bg mt-10"><span>Öğrenci Paneli</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- End Navbar ====
    ======================================= -->


<!-- =====================================
    ==== Start Header -->

<header class="header slider-fade" data-scroll-index="0">
    <div class="container-fluid">
        <div class="row">
            <div class="owl-carousel owl-theme full-width">
                <div class="text-center item bg-img" data-overlay-dark="7" data-background="img/slide1.jpg">
                    <div class="v-middle caption stwo mt-50">
                        <div class="o-hidden">

                            <h1>Sertifika Denizi</h1>
                            <p>Eğitimin Doğru Adresi.</p>
                            <a href="{{ route('ogrenci.index') }}" class="butn butn-light mt-30">
                                <span>Öğrenci Paneli</span>
                            </a>
                            <a href="#" data-scroll-nav="6" class="butn butn-bg mt-30">
                                <span style="color:#fff">Hemen Başvur</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="text-center item bg-img" data-overlay-dark="7" data-background="img/b2.jpg">
                    <div class="v-middle caption stwo mt-50">
                        <div class="o-hidden">
                            <h1>Sertifika Denizi</h1>
                            <p>Eğitimin Doğru Adresi.</p>
                            <a href="{{ route('ogrenci.index') }}" class="butn butn-light mt-30">
                                <span>Öğrenci Paneli</span>
                            </a>
                            <a href="#" data-scroll-nav="6" class="butn butn-bg mt-30">
                                <span style="color:#fff">Hemen Başvur</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="text-center item bg-img" data-overlay-dark="7" data-background="img/b3.jpg">
                    <div class="v-middle caption stwo mt-50">
                        <div class="o-hidden">
                            <h1>Sertifika Denizi</h1>
                            <p>Eğitimin Doğru Adresi.</p>
                            <a href="{{ route('ogrenci.index') }}" class="butn butn-light mt-30">
                                <span>Öğrenci Paneli</span>
                            </a>
                            <a href="#" data-scroll-nav="6" class="butn butn-bg mt-30">
                                <span style="color:#fff">Hemen Başvur</span>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>

<!-- End Header ====
    ======================================= -->


<!-- =====================================
    ==== Start Hero -->

<section class="hero section-padding" data-scroll-index="1">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                <div class="intro text-center mb-80">
                    <h6>Sertifika Denizinde</h6>
                    <h5>EĞİTİCİNİN EĞİTİMİ NEDİR?</h5>
                    <p>Bu eğitimimiz, uzman olduğu konuda eğitmen olmak isteyen veya mevcut işinde zaman zaman eğitim vermek durumunda olan bireylerin eğitim sürecini tasarlayıp
                        yönetebilmesini ve nitelikli bir eğitici olmasını sağlamayı hedeflemektedir.</p>
                </div>
            </div>

            <div class="feat-left col-lg-12">
                <div class="row">

                    <div class="col-lg-4">
                        <div class="item mb-md50">

                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="item mb-md50">
                            <span class="icon fas fa-clock"></span>

                            <p>Eğitimimiz 45 saat sürecektir. Eğitim sonunda başarılı olan kişiler ve M.E.B. onaylı Eğitici Eğitimi Sertifikası alacaktır.
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="item">

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- End Hero ====
    ======================================= -->


<!-- =====================================
    ==== Start get-ready

<section class="ready section-padding bg-img parallaxie" data-overlay-dark="3" data-background="img/bg2.jpg">
    <div class="container">
        <div class="row">

            <div class="offset-lg-2 col-lg-8 offset-md-1 col-md-10">
                <div class="video text-center">
                    <a class="vid" href="https://youtu.be/16CWP9dmTv0">
                            <span class="vid-butn">
                                <i class="icofont-ui-play"></i>
                            </span>
                    </a>
                </div>
                <div class="content text-center">
                    <h3>Eğitici Eğitimi</h3>
                    <p>Tanıtım videosu için üstteki butona tıklayabilirsiniz ...</p>

                </div>
            </div>

        </div>
    </div>

</section>

<!-- End get-ready ====
        ======================================= -->


<!-- =====================================
    ==== Start Portfolio -->

<section class="portfolio section-padding" data-scroll-index="2">
    <div class="container">
        <div class="row">

            <div class="section-head col-lg-12 col-md-10 ">
                <div class="row">
                    <div class="col-lg-5">
                        <h6>Sertifika Denizi</h6>
                        <h4>Eğitici Eğitimi</h4>
                    </div>
                    <div class="col-lg-7">
                        <p>Ayrıca etkin sunum yapmak ve topluluk önünde konuşmak isteyen kişilere bilgi, beceri ve bakış açısı kazandırmayı da amaçlamaktadır. Modern çağ insanının
                            ilgisini çekecek,motivasyonunu diri tutacak, çağdaş eğitim yöntem ve araçlarını işlevsel şekilde kullanabilecek, iletişimi yüksek, sınıf yönetimi güçlü
                            ve öğretme becerisi üst düzey eğitmenler yetiştirmek amacıyla hazırlanmış bir programdır.

                            .
                        </p>
                    </div>
                </div>
            </div>
            <div class="clearfix col-lg-12"></div>
            <!-- gallery -->
            <div class="gallery full-width">

                <!-- gallery item -->
                <div class="items graphic">
                    <div class="item-img">
                        <img src="img/portfolio/1.jpg" alt="image">
                        <div class="item-img-overlay">
                            <div class="overlay-info full-width">
                                <h6>Eğitici Eğitimi</h6>
                                <a href="img/portfolio/1.jpg" class="popimg">
                                    <span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- gallery item -->
                <div class="items web">
                    <div class="item-img">
                        <img src="img/portfolio/2.jpg" alt="image">
                        <div class="item-img-overlay">
                            <div class="overlay-info full-width">
                                <h6>Eğitici Eğitimi</h6>
                                <a href="img/portfolio/2.jpg" class="popimg">
                                    <span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- gallery item -->
                <div class="items brand">
                    <div class="item-img">
                        <img src="img/portfolio/3.jpg" alt="image">
                        <div class="item-img-overlay">
                            <div class="overlay-info full-width">
                                <h6>Eğitici Eğitimi</h6>
                                <a href="img/portfolio/3.jpg" class="popimg">
                                    <span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- gallery item -->
                <div class="items graphic">
                    <div class="item-img">
                        <img src="img/portfolio/4.jpg" alt="image">
                        <div class="item-img-overlay">
                            <div class="overlay-info full-width">
                                <h6>Eğitici Eğitimi</h6>
                                <a href="img/portfolio/4.jpg" class="popimg">
                                    <span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- gallery item -->
                <div class="items web">
                    <div class="item-img">
                        <img src="img/portfolio/5.jpg" alt="image">
                        <div class="item-img-overlay">
                            <div class="overlay-info full-width">
                                <h6>Eğitici Eğitimi</h6>
                                <a href="img/portfolio/5.jpg" class="popimg">
                                    <span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- gallery item -->
                <div class="items brand">
                    <div class="item-img">
                        <img src="img/portfolio/6.jpg" alt="image">
                        <div class="item-img-overlay">
                            <div class="overlay-info full-width">
                                <h6>Eğitici Eğitimi</h6>
                                <a href="img/portfolio/6.jpg" class="popimg">
                                    <span class="icon"><i class="fas fa-long-arrow-alt-right"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

<!-- End Portfolio ====
    ======================================= -->


<!-- =====================================
    ==== Start Section-box -->

<section class="section-box bg-img" data-overlay-dark="4" data-background="img/b5.jpg" data-scroll-index="1">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-5 offset-lg-1 half-content bg-gray">
                <div class="box-white">

                    <div class="content mb-50">
                        <h5 class="mb-15">Neden Eğitici Eğitimi ?</h5>
                        <p>Eğitici Eğitimi katılım sağlamak isteyen bireyler için eğitim kurumumuzda pek çok avantaj bulunmaktadır. Öncelikle Eğitici Eğitimi online eğitim
                            sayesinde Türkiye’nin her yerinden kurslarımıza katılım gösterebilirsiniz</p>
                        <p>Bunun için sistemimize kayıt olmak yeterlidir. Eğitim süresi boyunca derslere kaliteli şekilde katılım gösteren kursiyerlerimizin insan kaynakları ve
                            eğitimleri konusunda kendilerini geliştirmeleri mümkündür.</p>

                    </div>
					<div class="col-lg-12 offset-lg-3 offset-md-3">
					<img class="mt-10 text-center" style="width:40%;" src="img/meb_logo.jpg">
					</div>
                    <!-- Skills

                    <div class="skills">
                        <div class="skill-item">
                            <h6>Web Design</h6>
                            <div class="skill-progress">
                                <div class="progres" data-value="90%"></div>
                            </div>
                        </div>
                        <div class="skill-item">
                            <h6>Branding</h6>
                            <div class="skill-progress">
                                <div class="progres" data-value="75%"></div>
                            </div>
                        </div>
                        <div class="skill-item">
                            <h6>Development</h6>
                            <div class="skill-progress">
                                <div class="progres" data-value="80%"></div>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>

            <div class="col-lg-6 half-img bgimg-height valign">
                <div class="full-width">
                    <div class="offset-md-3 col-md-6">
                        <div class="quote">
                            <div class="icon"><i class="fas fa-quote-left"></i></div>
                            <p>Çünkü eğitim kurumumuz sertifikasını sınavları geçen her katılımcısına vermektedir.</p>
                            <span style="color: #fff">" Sertifika Denizi "</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- End Section-box ====
    ======================================= -->


<!-- =====================================
    ==== Start Featurse -->

<section class="featurse section-padding" data-scroll-index="3">
    <div class="container">
        <div class="row">

            <div class="section-head col-lg-12">
                <div class="row">
                    <div class="col-lg-4">
                        <h6>Kurs Eğitmenimiz</h6>
                        <h4>Kübra Şahin</h4>
                    </div>
                    <div class="col-lg-8">
                        <p>Sertifika Denizi'nın sunduğu eğitimler sayesinde eğitmenimiz tüm eğitici eğitimiyle alakalı konuları ve deneyimlerini sizlere seve seve aktaracaktır.
                        </p>
                    </div>
                </div>
            </div>


           <!--<div class="col-lg-7 content">
                <div class="item mb-30">
                    <span class="icon"><i class="fas fa-user"></i></span>
                    <div class="cont">
                        <h6>Hakkında</h6>
                        <p>Birçok saygın kurumda eğitmenlik, danışmanlık, Türkçe ve Edebiyat bölüm başkanlıkları gibi görevler almıştır.
                            Eğitmen, koç, konuşmacı ve danışman olarak bireylere ve kurumlara yönelik gelişim ve motivasyon süreçleri tasarlamakta ve yönetmekte olan Umut Esen’in
                            uzmanlıkları şunlardır;</p>
                    </div>
                </div>
                <div class="item active mb-30">
                    <span class="icon"><i class="icofont-magic"></i></span>
                    <div class="cont">

                        <p>- Paul Ekman International onaylı Mikro Mimik uzmanı</p>

                        <p> - Duygusal Zeka Uzmanı</p>

                        <p>- Mülakat Değerlendirme Uzmanı</p>

                        <p>- NLP Master</p>
                    </div>
                </div>
                <div class="item  mb-30">
                    <span class="icon"><i class="icofont-papers"></i></span>
                    <div class="cont">

                        <p>“Tolstoy’un Bisikleti” kitabının yazarıdır. PCC unvanlı profesyonel koçtur.

                        <p>Birçok üniversitenin yanı sıra AIESEC Türkiye, BJK Futbol Akademisi, Art of Kitchen, Atölye Eğitim, RHEA Girişim gibi kurumların bünyesinde eğitim ve
                            seminerler vermiş;birçok organizasyonda konuşmacı olarak yer almıştır.

                    </div>
                </div>

            </div>-->

            <!--<div class="col-lg-5 fimg">
                <div class="owl-carousel owl-theme">
                    <img src="img/2.jpg" alt="">
                </div>
            </div>-->

        </div>
    </div>
</section>

<!-- End Featurse ====
    ======================================= -->


<!-- =====================================
    ==== Start Price

<section class="price section-padding bg-gray" data-scroll-index="4">
    <div class="container">
        <div class="row">

            <div class="section-head col-lg-8 col-md-10 offset-md-1 offset-lg-2">
                <div class="row">
                    <div class="col-lg-5 text-right">
                        <h6>Pricing Table</h6>
                        <h4>Pricing .</h4>
                    </div>
                    <div class="col-lg-6 offset-lg-1">
                        <p>Nulla metus metus ullamcorper vel tincidunt sed euismod nibh Quisque volutpat condime.
                        </p>
                    </div>
                </div>
            </div>
            <div class="clearfix col"></div>

            <div class="pricing-tables text-center full-width">

                <div class="row">

                    <div class="col-lg-3 col-md-6">
                        <div class="item mb-md50">
                            <div class="type">
                                <h4>Free</h4>
                            </div>

                            <div class="value">
                                <h3><span>$</span>00</h3>
                                <span class="per">/ Month</span>
                            </div>

                            <div class="features">
                                <ul>
                                    <li>10 GB Disk Space</li>
                                    <li>15 Domain Names</li>
                                    <li>4 Email Address</li>
                                    <li>Enhanced Security</li>
                                    <li>Unlimited Support</li>
                                </ul>
                            </div>

                            <div class="order">
                                <a href="#0" class="butn butn-bg">
                                    <span>Order Now</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="item mb-md50">
                            <div class="type">
                                <h4>Basic</h4>
                            </div>

                            <div class="value">
                                <h3><span>$</span>10</h3>
                                <span class="per">/ Month</span>
                            </div>

                            <div class="features">
                                <ul>
                                    <li>10 GB Disk Space</li>
                                    <li>15 Domain Names</li>
                                    <li>4 Email Address</li>
                                    <li>Enhanced Security</li>
                                    <li>Unlimited Support</li>
                                </ul>
                            </div>

                            <div class="order">
                                <a href="#0" class="butn butn-bg">
                                    <span>Order Now</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="item active mb-sm50">

                            <div class="type">
                                <h4>Standard</h4>
                            </div>

                            <div class="value">
                                <h3><span>$</span>30</h3>
                                <span class="per">/ Month</span>
                            </div>

                            <div class="features">
                                <ul>
                                    <li>100 GB Disk Space</li>
                                    <li>30 Domain Names</li>
                                    <li>5 Email Address</li>
                                    <li>Enhanced Security</li>
                                    <li>Unlimited Support</li>
                                </ul>
                            </div>

                            <div class="order">
                                <a href="#0" class="butn butn-light">
                                    <span>Order Now</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="item">
                            <div class="type">
                                <h4>Premium</h4>
                            </div>

                            <div class="value">
                                <h3><span>$</span>80</h3>
                                <span class="per">/ Month</span>
                            </div>

                            <div class="features">
                                <ul>
                                    <li>500 GB Disk Space</li>
                                    <li>100 Domain Names</li>
                                    <li>10 Email Address</li>
                                    <li>Enhanced Security</li>
                                    <li>Unlimited Support</li>
                                </ul>
                            </div>

                            <div class="order">
                                <a href="#0" class="butn butn-bg">
                                    <span>Order Now</span>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>

End Price ====
    ======================================= -->


<!-- =====================================
    ==== Start Testimonials

<section class="testimonials section-padding">
    <div class="container">
        <div class="row">

            <div class="offset-lg-2 col-lg-8 offset-md-1 col-md-10">
                <div class="owl-carousel owl-theme text-center">
                    <div class="item">
                        <div class="client-area">
                            <div class="img">
                                <span class="icon"><img src="img/left-quote.svg" alt=""></span>
                                <span class="icon"><img src="img/right-quote.svg" alt=""></span>
                                <div class="author">
                                    <img src="img/clients/1.jpg" alt="">
                                </div>
                            </div>
                            <h6>Alex Smith</h6>
                            <span>Envato Customer</span>
                        </div>
                        <p>Nulla metus metus ullamcorper vel tincidunt sed euismod nibh Quisque volutpat condimentum
                            velit class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                    </div>
                    <div class="item">
                        <div class="client-area">
                            <div class="img">
                                <span class="icon"><img src="img/left-quote.svg" alt=""></span>
                                <span class="icon"><img src="img/right-quote.svg" alt=""></span>
                                <div class="author">
                                    <img src="img/clients/2.jpg" alt="">
                                </div>
                            </div>
                            <h6>Sam Smith</h6>
                            <span>Envato Customer</span>
                        </div>
                        <p>Nulla metus metus ullamcorper vel tincidunt sed euismod nibh Quisque volutpat condimentum
                            velit class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                    </div>
                    <div class="item">
                        <div class="client-area">
                            <div class="img">
                                <span class="icon"><img src="img/left-quote.svg" alt=""></span>
                                <span class="icon"><img src="img/right-quote.svg" alt=""></span>
                                <div class="author">
                                    <img src="img/clients/3.jpg" alt="">
                                </div>
                            </div>
                            <h6>Alex Martin</h6>
                            <span>Envato Customer</span>
                        </div>
                        <p>Nulla metus metus ullamcorper vel tincidunt sed euismod nibh Quisque volutpat condimentum
                            velit class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- End Testimonials ====
    ======================================= -->


<!-- =====================================
    ==== Start Numbers -->

<div class="numbers section-padding bg-img parallaxie" data-overlay-dark="7" data-background="img/b3.jpg">
   <!-- <div class="container">
        <div class="row">

            <div class="col-lg-4 col-md-6">
                <div class="item text-center mb-md50">
                    <span class="icon fas fa-user"></span>
                    <h3 class="count">300</h3>
                    <h6>den Fazla Kursiyerimiz</h6>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="item text-center mb-md50">
                    <span class="icon fas fa-video"></span>
                    <h3 class="count">11</h3>
                    <h6>Ders Videosu</h6>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="item text-center mb-sm50">
                    <span class="icon fas fa-clock"></span>
                    <h3 class="count">40</h3>
                    <h6>Saat Eğitim</h6>
                </div>
            </div>


        </div>
    </div>-->
</div>

<!-- End Numbers ====
    ======================================= -->


<!-- =====================================
    ==== Start Team

<section class="team section-padding">
    <div class="container">
        <div class="row">

            <div class="section-head col-lg-8 col-md-10 offset-md-1 offset-lg-2">
                <div class="row">
                    <div class="col-lg-5 text-right">
                        <h6>Creative Minds</h6>
                        <h4>Our Team .</h4>
                    </div>
                    <div class="col-lg-6 offset-lg-1">
                        <p>Nulla metus metus ullamcorper vel tincidunt sed euismod nibh Quisque volutpat condime.
                        </p>
                    </div>
                </div>
            </div>
            <div class="clearfix col"></div>

            <div class="col-lg-3 col-md-6">
                <div class="item mb-md50">
                    <div class="img">
                        <img src="img/team/1.jpg" alt="">
                    </div>
                    <div class="info">
                        <h6>Alex Smith</h6>
                        <p>UI/UX Designer</p>
                        <div class="social valign">
                            <div class="full-width">
                                <a href="#0"><i class="icofont-facebook"></i></a>
                                <a href="#0"><i class="icofont-twitter"></i></a>
                                <a href="#0"><i class="icofont-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="item mb-md50">
                    <div class="img">
                        <img src="img/team/2.jpg" alt="">
                    </div>
                    <div class="info">
                        <h6>Alex Smith</h6>
                        <p>UI/UX Designer</p>
                        <div class="social valign">
                            <div class="full-width">
                                <a href="#0"><i class="icofont-facebook"></i></a>
                                <a href="#0"><i class="icofont-twitter"></i></a>
                                <a href="#0"><i class="icofont-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="item mb-sm50">
                    <div class="img">
                        <img src="img/team/3.jpg" alt="">
                    </div>
                    <div class="info">
                        <h6>Alex Smith</h6>
                        <p>UI/UX Designer</p>
                        <div class="social valign">
                            <div class="full-width">
                                <a href="#0"><i class="icofont-facebook"></i></a>
                                <a href="#0"><i class="icofont-twitter"></i></a>
                                <a href="#0"><i class="icofont-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="item">
                    <div class="img">
                        <img src="img/team/4.jpg" alt="">
                    </div>
                    <div class="info">
                        <h6>Alex Smith</h6>
                        <p>UI/UX Designer</p>
                        <div class="social valign">
                            <div class="full-width">
                                <a href="#0"><i class="icofont-facebook"></i></a>
                                <a href="#0"><i class="icofont-twitter"></i></a>
                                <a href="#0"><i class="icofont-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- End Team ====
    ======================================= -->


<!-- =====================================
    ==== Start Blog

<section class="blog section-padding bg-gray" data-scroll-index="5">
    <div class="container">
        <div class="row">
            <div class="section-head col-lg-8 col-md-10 offset-md-1 offset-lg-2">
                <div class="row">
                    <div class="col-lg-5 text-right">
                        <h6>Latest News</h6>
                        <h4>Our Blog .</h4>
                    </div>
                    <div class="col-lg-6 offset-lg-1">
                        <p>Nulla metus metus ullamcorper vel tincidunt sed euismod nibh Quisque volutpat condime.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-4">
                <div class="item mb-sm50 wow fadeInUp mb-md50" data-wow-delay=".1s">
                    <div class="post-img">
                        <div class="img">
                            <img src="img/blog/1.jpg" alt="">
                        </div>
                    </div>
                    <div class="cont">
                        <div class="info">
                            <a href="#0">Alex Smith</a>
                            <a href="#0">August 06 2019</a>
                        </div>

                        <h5><a href="#0">Master These Awesome New Skills in May 2019</a></h5>

                        <a href="#0" class="more">
                            <span>Read More <i class="icofont-caret-right"></i></span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="item wow fadeInUp mb-md50" data-wow-delay=".3s">
                    <div class="post-img">
                        <div class="img">
                            <img src="img/blog/2.jpg" alt="">
                        </div>
                    </div>
                    <div class="cont">
                        <div class="info">
                            <a href="#0">Alex Smith</a>
                            <a href="#0">August 06 2019</a>
                        </div>

                        <h5><a href="#0">48 Best WordPress Themes On Envato</a></h5>

                        <a href="#0" class="more">
                            <span>Read More <i class="icofont-caret-right"></i></span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="item wow fadeInUp" data-wow-delay=".5s">
                    <div class="post-img">
                        <div class="img">
                            <img src="img/blog/3.jpg" alt="">
                        </div>
                    </div>
                    <div class="cont">
                        <div class="info">
                            <a href="#0">Alex Smith</a>
                            <a href="#0">August 06 2019</a>
                        </div>

                        <h5><a href="#0">48 Best WordPress Themes On Envato</a></h5>

                        <a href="#0" class="more">
                            <span>Read More <i class="icofont-caret-right"></i></span>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

End Blog ====
    ======================================= -->


<!-- =====================================
    ==== Start Contact -->

<section class="contact section-padding bg-img" data-overlay-light="8" data-background="img/contact-map.jpg"
         data-scroll-index="6">
    <div class="container">
        <div class="row">
            <div class="section-head col-lg-8 col-md-10 offset-md-1 offset-lg-2">
                <div class="row">
                    <div class="col-lg-12 text-center">

                        <h4>Hızlı Başvur</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5">
                <div class="info mb-md50">

                    <div class="item wow fadeIn" data-wow-delay=".3s">
                        <h6></h6>
                        <h3>Sertifika Denizi</h3>
                        <p><span></span></p>

                        <h4></h4>
                        <p><a href="#0"></a></p>
                    </div>
                </div>
            </div>
            <!-- Contact Form -->
            <div class="col-lg-7">
                <form class="form wow fadeIn" data-wow-delay=".5s" id="contact-form" method="post"
                      action="#">

                    <div class="messages"></div>

                    <div class="controls">

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input id="form_name" type="text" name="name" placeholder="Adınız Soyadınız *"
                                           required="required" data-error="İsim Boş Olamaz.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input id="form_email" type="text" name="phone" placeholder="Telefonunuz *"
                                           required="required" data-error="Telefon Boş Olamaz.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input id="form_email" type="text" name="email" placeholder="E-posta Adresiniz *"
                                           required="required" data-error="E-posta Adresi Boş Olamaz.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                        <textarea id="form_message" name="message" placeholder="Mesajınız *" rows="4"
                                                  required="required" data-error="Mesaj Boş Olamaz."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="butn butn-bg"><span>Başvur <i class="icofont-paper-plane"></i></span></button>
                            </div>

                        </div>
                    </div>
                    <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
                    <input type="hidden" name="action" value="validate_captcha">
                </form>
            </div>

        </div>
    </div>
</section>

<!-- End Contact ====
    ======================================= -->


<!-- =====================================
    ==== Start Footer -->

<footer class="text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">

                <img class="white-img" src="img/Sertifika-dunyas.png" alt="logo">

            </div>
            <div class="col-lg-6">
                <div class="social">
                    <a href="#0"><i class="icofont-facebook"></i></a>
                    <a href="#0"><i class="icofont-twitter"></i></a>
                    <a href="#0"><i class="icofont-instagram"></i></a>
                    <a href="#0"><i class="icofont-linkedin"></i></a>
                    <a href="#0"><i class="icofont-behance"></i></a>
                </div>


                <p>&copy; 2022 Sertifika Denizi. Tüm Hakları Saklıdır.<br> | Designed by <a style="color:white;" href="https://statikyazilim.com.tr" target="_blank"> Statik
                        Yazılım</a></p>
            </div>
        </div>
    </div>
</footer>

<!-- End Footer ====
    ======================================= -->


<!-- jQuery -->
<script src="js/jquery-3.0.0.min.js"></script>
<script src="js/jquery-migrate-3.0.0.min.js"></script>

<!-- popper.min -->
<script src="js/popper.min.js"></script>

<!-- bootstrap -->
<script src="js/bootstrap.min.js"></script>

<!-- scrollIt -->
<script src="js/scrollIt.min.js"></script>

<!-- jquery.hover3d.min -->
<script src="js/jquery.hover3d.min.js"></script>

<!-- jquery.waypoints.min -->
<script src="js/jquery.waypoints.min.js"></script>

<!-- jquery.counterup.min -->
<script src="js/jquery.counterup.min.js"></script>

<!-- circle-progress.min -->
<script src="js/circle-progress.min.js"></script>

<!-- owl carousel -->
<script src="js/owl.carousel.min.js"></script>

<!-- jquery.magnific-popup js -->
<script src="js/jquery.magnific-popup.min.js"></script>

<!-- parallaxie js -->
<script src="js/parallaxie.js"></script>

<!-- isotope.pkgd.min js -->
<script src="js/isotope.pkgd.min.js"></script>

<!-- YouTubePopUp.jquery -->
<script src="js/YouTubePopUp.jquery.js"></script>

<!-- validator js -->
<script src="js/validator.js"></script>
<script src="https://www.google.com/recaptcha/api.js?render=6LfIfzYfAAAAAALAFsxcSFVT54yPIE1WYY1YTUz0"></script>

<!-- custom scripts -->
<script src="js/scripts.js"></script>

</body>


</html>
