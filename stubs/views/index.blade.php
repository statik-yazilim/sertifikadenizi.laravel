@extends('admin.app')
<?php /* @var \App\Models\Admin\Announcement[] $announcements */ ?>
@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Bizden Haberler</h2>
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="user-add black-bg">
                <div class="form-one">
                    <div class="section-heading">
                        <a data-toggle="collapse" href="#collapseAddForm" role="button" aria-expanded="{{ $errors->any() ? 'true' : 'false' }}" aria-controls="collapseAddForm">
                            Haber Ekle
                        </a>
                    </div>
                    <form action="{{ route('admin.announcements.store') }}" method="post" enctype="multipart/form-data" id="collapseAddForm"
                          class="collapse{{ $errors->any() ? ' show' : ''}}">
                        @csrf
                        <div class="row">
                            <x-upload-file id="image" width="444" height="287" model="\App\Models\Admin\Article" attr="image"></x-upload-file>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Başlık</label>
                                    <input type="text" name="name" id="name" class="style-one" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="slug">SEO URL</label>
                                    <input type="text" name="slug" id="slug" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">Özet İçerik</label>
                                    <textarea id="description" name="description" class="summernote"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="content">İçerik</label>
                                    <textarea id="content" name="content" class="summernote"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="ekle">
                                <input type="submit" value="Ekle" class="btn-one">
                            </div>
                        </div>
                        <input type="hidden" name="type" value="announcement">
                    </form>
                </div>
            </div>
            <div class="table-one black-bg">
                <div class="section-heading">
                    Haber Tablosu
                </div>
                <table class="datatable table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Başlık</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($announcements as $announcement)
                        <tr>
                            <td>@if ($announcement->files->isNotEmpty()) <img src="{{ asset('storage/' . $announcement->files->first()->path) }}" alt=""> @endif</td>
                            <td>{{ $announcement->name }}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.announcements.edit', $announcement) }}" class="btn-one edit-btn">Düzenle</a>
                                <a href="javascript:void(0)" class="btn-one delete-btn" onclick="deleteSubItem(this)"
                                   data-url="{{ route('admin.announcements.destroy', $announcement) }}">Sil</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection
