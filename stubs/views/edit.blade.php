@extends('admin.app')

@section('content')
    <div class="right-side">
        <section class="dashboard-content users">
            <div class="panel-heading">
                <h2><i class="fas fa-sign-in-alt"></i>Haber Düzenle</h2>
            </div>
            <div class="user-add black-bg">
                <div class="form-one">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.announcements.update', $announcement) }}" method="post">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <x-upload-file id="image" width="444" height="287"
                                           model="\App\Models\Admin\Announcement" attr="image"
                                           :selected-ids="$announcement->files->isNotEmpty() ? $announcement->files->first()->id : null"
                                           :model-id="$announcement->id"
                            ></x-upload-file>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Başlık</label>
                                    <input type="text" name="name" id="name"
                                           value="{{ $announcement->name }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="slug">SEO URL</label>
                                    <input type="text" name="slug" id="slug"
                                           value="{{ $announcement->slug }}" class="style-one">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">Özet İçerik</label>
                                    <textarea id="description" name="description" class="summernote">{{ $announcement->description }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="content">İçerik</label>
                                    <textarea id="content" name="content" class="summernote">{{ $announcement->content }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <input type="hidden" name="form" value="duzenle">
                                <input type="submit" value="Düzenle" class="btn-one">
                            </div>
                        </div>
                        <input type="hidden" name="type" value="announcement">
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
