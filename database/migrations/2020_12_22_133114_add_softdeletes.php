<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftdeletes extends Migration
{
    private $softDeleteTables = [
        'courses',
        'exams',
        'users'
    ];
    public function up()
    {
        foreach ($this->softDeleteTables as $table_name)
        {
            Schema::table($table_name, function(Blueprint $table) {
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        //
    }
}
