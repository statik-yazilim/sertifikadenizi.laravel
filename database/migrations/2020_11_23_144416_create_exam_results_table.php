<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamResultsTable extends Migration
{
    public function up()
    {
        Schema::create('exam_results', function (Blueprint $table) {
            $table->id();
            $table->foreignId('exam_instance_id')->constrained('exam_instances');
            $table->foreignId('user_id')->constrained('users');
            $table->integer('true_answer_count')->nullable();
            $table->decimal('success_rate')->nullable();
            $table->boolean('is_success')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('exam_results');
    }
}
