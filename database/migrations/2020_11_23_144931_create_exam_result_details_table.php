<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamResultDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('exam_result_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('exam_result_id')->constrained('exam_results');
            $table->foreignId('exam_question_id')->constrained('exam_questions');
            $table->foreignId('exam_customer_answer_id')->nullable()->constrained('exam_answers');
            $table->foreignId('actual_true_answer_id')->constrained('exam_answers');
            $table->boolean('is_true')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('exam_result_details');
    }
}
