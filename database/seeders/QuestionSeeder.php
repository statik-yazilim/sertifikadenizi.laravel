<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $exam_id = \DB::table('exams')->insertGetId([
            'course_id' => 1,
            'name' => $faker->sentence(),
            'duration' => $faker->randomNumber(2),
            'percentage' => $faker->numberBetween(50, 60),
            'begin_date' => Carbon::now()->format('Y-m-d'),
            'end_date' => Carbon::now()->format('Y-m-d'),
            'status' => 1
        ]);

        for ($j = 0; $j < 10; $j++)
        {
            $id = \DB::table('exam_questions')->insertGetId([
                'question_text' => $faker->sentence(),
                'exam_id' => $exam_id
            ]);

            $rand = rand(0, 3);
            for ($i = 0; $i < 4; $i++)
            {
                \DB::table('exam_answers')->insert([
                    'exam_question_id' => $id,
                    'answer_text' => $faker->words(2, true) . ($rand == $i ? ' DOĞRU' : ''),
                    'is_true' => $rand == $i
                ]);
            }
        }
    }
}
