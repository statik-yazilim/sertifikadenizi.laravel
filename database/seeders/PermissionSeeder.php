<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\PermissionCategory;
use App\Models\Role;
use App\Models\User;
use DB;
use Illuminate\Database\Seeder;
use Schema;

class PermissionSeeder extends Seeder
{
    public $permissions = [
        'admins' => [
            'employees' => 'Personel İşlemleri',
            'roles' => 'Yetki İşlemleri',
            'admins' => 'Yönetici İşlemleri',
            'logs' => 'Kayıt İşlemleri',
        ],
        'employee' => [
            'clients' => 'Müvekkil İşlemleri',
            'client_payments' => 'Müvekkil Ödemeleri',

            'court_cases' => 'Dava Dosyaları İşlemleri',
            'enforcements' => 'İcra Dosyaları İşlemleri',
            'postExpenses' => 'Posta Masrafları İşlemleri',
            'officeExpenses' => 'Ofis Giderleri İşlemleri',

            'uyap_saleAdvances' => 'UYAP Satış Avansları',
            'uyap_valuationAdvances' => 'UYAP Kıymet Takdiri Avansları',
            'uyap_expertFees' => 'UYAP Bilirkişi Masraf Avansları',
            'uyap_prosecutionAdvances' => 'UYAP Takip ve Dava Açılış Avansları',
            'uyap_collectFeeAdvances' => 'UYAP Tahsil Harç Avansları',
            'uyap_distraintRouteFees' => 'UYAP Haciz Yolluk Harçları',
            'uyap_enotices' => 'UYAP E-Tebligat',
            'uyap_baroKartExpenses' => 'UYAP Baro Kart Yükleme Kesintileri',
            'uyap_expenseAdvances' => 'UYAP Gider Avansları',
            'uyap_advancePayments' => 'UYAP Aktarılan Avanslar',
            'uyap_balance' => 'UYAP Bakiye',
            'tasks' => 'İş Emirleri',
        ]
    ];

    public function run()
    {
        $tableNames = config('permission.table_names');
        Schema::disableForeignKeyConstraints();
        DB::table($tableNames['model_has_roles'])->truncate();
        DB::table($tableNames['model_has_permissions'])->truncate();
        DB::table($tableNames['role_has_permissions'])->truncate();
        DB::table($tableNames['roles'])->truncate();
        DB::table($tableNames['permissions'])->truncate();
        DB::table('permission_categories')->truncate();
        Schema::enableForeignKeyConstraints();

/*        foreach ($this->permissions as $r => $permission)
        {
            foreach ($permission as $name => $description)
            {
                $category = new PermissionCategory;
                $category->name = $description;
                $category->save();

                Permission::firstOrCreate(['name' => 'view_' . $name, 'description' => $description . ' Görüntüle', 'guard_name' => '*', 'permission_category_id' => $category->id]);
                if (!in_array($name, ['uyap_balance', 'logs']))
                {
                    Permission::firstOrCreate(['name' => 'create_' . $name, 'description' => $description . ' Oluştur', 'guard_name' => '*', 'permission_category_id' => $category->id]);
                    Permission::firstOrCreate(['name' => 'update_' . $name, 'description' => $description . ' Düzenle', 'guard_name' => '*', 'permission_category_id' => $category->id]);
                    Permission::firstOrCreate(['name' => 'destroy_' . $name, 'description' => $description . ' Sil', 'guard_name' => '*', 'permission_category_id' => $category->id]);
                }
            }
        }*/

        $role = Role::firstOrCreate(['name' => 'Admin', 'guard_name' => '*']);
        $role->syncPermissions(Permission::all());
        Role::firstOrCreate(['name' => 'Üye', 'guard_name' => '*']);
    }
}
