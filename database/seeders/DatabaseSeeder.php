<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Provider\Lorem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionSeeder::class
        ]);

        $user = new User([
            'name' => 'admin', 'email' => 'y@gizcan.xyz', 'password' => '1234'
        ]);
        $user->save();

        $user->assignRole('Admin');

        $user = new User([
            'name' => 'demo', 'email' => 'demo@statikyazilim.com.tr', 'password' => '1234'
        ]);
        $user->save();

        $user->assignRole('Üye');
    }
}
