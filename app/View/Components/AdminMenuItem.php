<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdminMenuItem extends Component
{
    public $title;
    public $url;
    public $icon;

    /**
     * AdminMenuItem constructor.
     * @param $title
     * @param $url
     * @param $icon
     */
    public function __construct($title, $url, $icon)
    {
        $this->title = $title;
        $this->url = $url;
        $this->icon = $icon;
    }

    public function render()
    {
        return view('components.admin-menu-item');
    }
}
