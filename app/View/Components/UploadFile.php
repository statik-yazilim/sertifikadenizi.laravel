<?php

namespace App\View\Components;

use App\Models\Upload\File;
use Illuminate\View\Component;

class UploadFile extends Component
{
    public $id;
    public $model;
    public $width;
    public $height;
    public $attr;
    public $selectedIds;
    public $files;
    public $modelId;

    public function __construct($id, $model, $attr, $modelId = '', $selectedIds = '', $width = '', $height = '')
    {
        $this->id = $id;
        $this->model = $model;
        $this->width = $width;
        $this->height = $height;
        $this->attr = $attr;
        $this->selectedIds = $selectedIds;
        $this->modelId = $modelId;

        if (!empty($this->selectedIds))
        {
            $this->files = File::find($this->selectedIds);
        }

    }

    public function render()
    {
        return view('components.upload-file', [
            'files' => $this->files ?? null
        ]);
    }
}
