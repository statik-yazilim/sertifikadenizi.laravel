<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Str;

class StoreAnnouncementRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if (empty($this->slug))
        {
            $this->merge([
                'slug' => Str::slug($this->name),
            ]);
        }
        else
        {
            $this->merge([
                'slug' => Str::slug($this->slug),
            ]);
        }
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'content' => 'required',
            'type' => 'required',
            'description' => 'nullable',
            'slug' => ''
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Başlık girilmedi',
            'content.required' => 'İçerik girilmedi'
        ];
    }
}
