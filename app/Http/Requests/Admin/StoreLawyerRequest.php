<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Str;

class StoreLawyerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'duty' => 'required|max:191',
            'image' => ''
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'İsim girilmedi.',
            'duty.required' => 'Görev girilmedi.'
        ];
    }
}
