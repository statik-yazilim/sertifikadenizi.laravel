<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Str;

class StoreCourseRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if (empty($this->slug))
        {
            $this->merge([
                'slug' => Str::slug($this->name),
            ]);
        }
        else
        {
            $this->merge([
                'slug' => Str::slug($this->slug),
            ]);
        }
    }

    public function rules()
    {
        return [
            'name' => ['required', 'max:191'],
            'description' => ['nullable'],
            'agreement' => ['nullable'],
            'image' => '',
            'slug' => ''
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Başlık girilmedi',
        ];
    }
}
