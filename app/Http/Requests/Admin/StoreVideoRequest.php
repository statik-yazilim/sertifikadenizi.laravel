<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreVideoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => ['required', 'max:191'],
            'link' => ['required', 'max:191'],
            'course_id' => ['required', 'exists:courses,id']
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Başlık girilmedi',
            'link.required' => 'Video linki girilmedi',
            'course_id.required' => 'Kurs girimedi',
            'course_id.exists' => 'Kurs bulunamadı'
        ];
    }
}
