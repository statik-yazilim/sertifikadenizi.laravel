<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreExamRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'status' => isset($this->status)
        ]);
    }

    public function rules()
    {
        return [
            'name' => ['required'],
            'percentage' => 'integer',
            'duration' => 'integer',
            'end_date' => '',
            'begin_date' => '',
            'status' => 'boolean',
            'course_id' => ['exists:courses,id']
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Başlık girilmedi',
            'percentage.integer' => 'Geçer yüzdesi bir sayı olmalı',
            'duration.integer' => 'Sınav süresi bir sayı olmalı',
            'course_id.exists' => 'Kurs bulunamadı',
        ];
    }
}
