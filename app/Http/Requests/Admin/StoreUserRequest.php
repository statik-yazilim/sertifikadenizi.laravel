<?php


namespace App\Http\Requests\Admin;


use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
//        dd($this);
        return [
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['sometimes'],
            'tc_no' => ['sometimes', 'nullable', 'integer'],
            'role' => ''
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'İsim girilmedi',
            'email.required' => 'E-posta',
            'email.email' => 'Geçersiz e-posta',
            'password.required' => 'Şifre girilmedi',
            'tc_no.integer' => 'TC No rakamlardan oluşmalı'
        ];
    }
}
