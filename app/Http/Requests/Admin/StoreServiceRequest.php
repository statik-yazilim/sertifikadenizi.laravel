<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Str;

class StoreServiceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if (empty($this->slug))
        {
            $this->merge([
                'slug' => Str::slug($this->name),
            ]);
        }
        else
        {
            $this->merge([
                'slug' => Str::slug($this->name),
            ]);
        }
    }

    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'description' => 'required',
            'slug' => ''
        ];
    }
}
