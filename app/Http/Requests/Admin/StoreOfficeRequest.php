<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Str;

class StoreOfficeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if (empty($this->slug))
        {
            $this->merge([
                'slug' => Str::slug($this->name),
            ]);
        }
        else
        {
            $this->merge([
                'slug' => Str::slug($this->slug),
            ]);
        }
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => '',
            'content' => 'required',
            'description' => 'nullable',
            'address' => 'nullable',
            'email' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'İsim girilmedi.',
            'content.required' => 'İçerik girilmedi.'
        ];
    }
}
