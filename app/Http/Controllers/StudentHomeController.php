<?php

namespace App\Http\Controllers;


class StudentHomeController extends Controller
{
    public function index()
    {
        return view('student.home');
    }
}
