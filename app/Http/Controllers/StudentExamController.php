<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin\Exam;
use App\Models\Admin\ExamAnswer;
use App\Models\Admin\ExamQuestion;
use App\Models\Admin\ExamStudent;
use App\Models\ExamResult;
use App\Models\ExamInstance;
use App\Models\ExamResultDetail;
use Auth;
use Debugbar;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class StudentExamController extends Controller
{
    public function index()
    {
        $exams = Exam::where(function (Builder $query) {
            $query->whereHas('students', function(Builder $query) {
                $query->where('user_id', Auth::user()->id);
            });
            $query->where('status', 1);
        })->orWhere(function (Builder $query) {
            $query->orWhereHas('results', function(Builder $query) {
                $query->where('user_id', Auth::user()->id);
            });
        })
        ->get();

        return view('student.exams.index', compact('exams'));
    }

    public function join(Exam $exam)
    {
        $examInstance = $exam->instance;

        return view('student.exams.join', compact('examInstance'));
    }

    public function finish(Request $request)
    {
        $examResult = new ExamResult;
        $examResult->exam_instance_id = $request->examInstanceId;
        $examResult->user_id = Auth::id();
        $examResult->true_answer_count = 0;
        $examResult->success_rate = 0;
        $examResult->save();

        $trueAnswerCount = 0;
        foreach ($request->examResultDetail as $detail) {
            $examTrueAnswer = ExamQuestion::find($detail['examQuestionId'])->answers->where('is_true')->first();
            $examResultDetail = new ExamResultDetail;

            $examResultDetail->exam_result_id = $examResult->id;
            $examResultDetail->actual_true_answer_id = $examTrueAnswer->id;
            $examResultDetail->exam_customer_answer_id = $detail['examCustomerAnswerId'];
            $examResultDetail->exam_question_id = $detail['examQuestionId'];

            if ($examTrueAnswer->id == $detail['examCustomerAnswerId'])
            {
                $examResultDetail->is_true = 1;
                $trueAnswerCount++;
            }
            $examResultDetail->save();
        }

        $examResult->true_answer_count = $trueAnswerCount;
        $examResult->success_rate = ($examResult->true_answer_count / ExamInstance::find($examResult->exam_instance_id)->exam->questions->count() * 100);
        $examResult->is_success = $examResult->success_rate >= ExamInstance::find($examResult->exam_instance_id)->exam->percentage;
        $examResult->ip_address = $request->ip();
        $examResult->update();

        return response()->json([
            'resultType' => 'ok'
        ]);
    }

    public function show(ExamInstance $examInstance)
    {
        $examResults = $examInstance->currentUserResult()->first();

        return view('student.exams.show', compact('examResults', 'examInstance'));
    }
}
