<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin\Course;
use Illuminate\Http\Request;

class StudentCoursesController extends Controller
{
    public function index()
    {
        $courses = Course::all();

        return view('student.courses.index', compact('courses'));
    }
}
