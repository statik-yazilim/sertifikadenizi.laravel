<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin\PersonalMessage;
use App\Models\Admin\PersonalMessageThread;
use Auth;
use Illuminate\Http\Request;

class StudentPersonalMessageController extends Controller
{
    public function index()
    {
        $thread = PersonalMessageThread::where('sender_id', Auth::id())->orWhere('receiver_id', Auth::id())->first();

        return view('student.ask.index', compact('thread'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'message' => 'required',
        ]);

        $thread = PersonalMessageThread::firstOrCreate([
            'sender_id' => Auth::id(),
            'receiver_id' => 1
        ]);

        $thread->personalMessages()->create([
            'body' => $validatedData['message'],
            'user_id' => Auth::id()
        ]);

        return back();
    }
}
