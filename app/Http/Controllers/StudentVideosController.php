<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin\Course;
use App\Models\Admin\Video;
use Illuminate\Http\Request;

class StudentVideosController extends Controller
{
    public function show(Course $course, Video $video)
    {
        $video->users()->syncWithoutDetaching(\Auth::user());
        return view('student.courses.show', compact('course', 'video'));
    }
}
