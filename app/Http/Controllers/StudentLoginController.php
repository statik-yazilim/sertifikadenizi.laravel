<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class StudentLoginController extends Controller
{
    public function login()
    {
        return view('student.login');
    }

    public function check(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, true))
        {
            if (Auth::user()->hasRole('Admin')) {
                return redirect()->route('admin.index');
            }
            return redirect()->route('ogrenci.index');
        }

        return redirect()->back()->withInput()->withErrors(['auth_failed' => 'Kullanıcı bilgisi yanlış.']);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('index');
    }
}
