<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Models\Mails\Contact;
use Illuminate\Http\Request;
use Mail;

class HomepageController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function email(Request $request)
    {
        if (empty($request->input('g-recaptcha-response'))) {
            return response()->json(['type' => 'danger', 'message' => 'Bir hata oluştu, lütfen daha sonra tekrar deneyiniz.']);
        }

        $secret  = '6LfIfzYfAAAAAGY9-XLkUTe3h8jQs3j3XJvtAy2D';
        $response = $request->input('g-recaptcha-response');

        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $response . '&remoteip=' . $_SERVER['REMOTE_ADDR'];
        $curl_response = json_decode(file_get_contents($url));

        if (empty($curl_response) || !$curl_response->success) {
            return response()->json(['type' => 'danger', 'message' => 'Bir hata oluştu, lütfen daha sonra tekrar deneyiniz.']);
        }

        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required',
        ], [
            'name.required' => 'İsim girilmedi',
            'email.required' => 'Email girilmedi',
            'phone.required' => 'Telefon girilmedi',
            'message.required' => 'Mesaj girilmedi',
        ]);

        $contact = new Contact($validatedData);

        Mail::to(config('mail.from.address'))
            ->send(new ContactMail($contact));

        return response()->json(['type' => 'success', 'message' => 'İletişim isteğiniz iletildi. En kısa sürede tarafınıza geri dönüş yapılacaktır.']);
    }
}

