<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreVideoRequest;
use App\Models\Admin\Course;
use App\Models\Admin\Video;

class VideoController extends Controller
{
    public function index()
    {
        $videos = Video::all();
        $courses = Course::all();
        return view('admin.videos.index', ['videos' => $videos, 'courses' => $courses]);
    }

    public function create()
    {
        return view('admin.videos.create');
    }

    public function store(StoreVideoRequest $request)
    {
        $validatedData = $request->validated();

        $video = new Video($validatedData);
        $video->save();

        return back()->withInput()->with('status', 'Başarıyla eklendi!');
    }

    public function show(Video $video)
    {
        //
    }

    public function edit(Video $video)
    {
        $courses = Course::all();
        return view('admin.videos.edit', ['video' => $video, 'courses' => $courses]);
    }

    public function update(StoreVideoRequest $request, Video $video)
    {
        $validatedData = $request->validated();

        $video->update($validatedData);

        return response()->redirectToRoute('admin.videos.index')->with('status', 'Başarıyla düzenlendi!');
    }

    public function destroy(Video $video)
    {
        $video->delete();

        return response()->json(['success' => true, 'message' => 'İşlem Başarılı']);
    }
}
