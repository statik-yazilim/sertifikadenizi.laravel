<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Exam;
use App\Models\Admin\ExamStudent;
use App\Models\ExamInstance;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class ExamInstanceController extends Controller
{
    public function index(Exam $exam)
    {
        $users = User::notRole('Admin')->get();

        return view('admin.exams.create', compact('exam', 'users'));
    }

    public function store(Request $request, Exam $exam)
    {
        if (empty($request->instance_id))
        {
            ExamInstance::create([
                'exam_id' => $exam->id,
                'status' => 0
            ]);
        }
        else
        {
            $exam->students()->delete();
        }

        if (!empty($request->joining))
        {
            foreach ($request->joining as $user_id => $what)
            {
                $user = User::find($user_id);
                ExamStudent::create([
                    'exam_instance_id' => $exam->instance->id,
                    'user_id' => $user->id,
                ]);
            }
        }

        return redirect()->route('admin.exams.index')->with('status', 'Başarıyla eklendi!');
    }
}
