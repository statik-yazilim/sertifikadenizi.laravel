<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\PersonalMessage;
use App\Models\Admin\PersonalMessageThread;
use Auth;
use Illuminate\Http\Request;

class PersonalMessageController extends Controller
{
    public function index()
    {
        $threads = PersonalMessageThread::whereReceiverId(Auth::id())->get();

        return view('admin.personalMessages.index', ['threads' => $threads]);
    }

    public function create()
    {
        return view('admin.personalMessages.create');
    }

    public function store(Request $request, PersonalMessageThread $thread)
    {
        $validatedData = $request->validate([
            'message' => 'required',
        ]);

        $thread->personalMessages()->create([
            'body' => $validatedData['message'],
            'user_id' => Auth::id()
        ]);

        return back()->withInput()->with('status', 'Başarıyla gönderildi!');
    }

    public function show(PersonalMessageThread $message) // aslında $thread olmalıydı ama kolay yolu bu ¯\_(ツ)_/¯
    {
        $thread = $message;

        return view('admin.personalMessages.show', ['thread' => $thread]);
    }

    public function edit(PersonalMessage $personalMessage)
    {
        return view('admin.personalMessages.edit', ['personalMessage' => $personalMessage]);
    }

    public function update(Request $request, PersonalMessage $personalMessage)
    {
        $validatedData = $request->validated();

        $personalMessage->update($validatedData);

        return response()->redirectToRoute('admin.personalMessages.index')->with('status', 'Başarıyla düzenlendi!');
    }

    public function destroy(PersonalMessageThread $message)
    {
        $message->delete();

        return response()->json(['success' => true, 'message' => 'İşlem Başarılı']);
    }
}
