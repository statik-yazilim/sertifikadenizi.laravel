<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Exam;
use App\Models\Admin\ExamAnswer;
use App\Models\Admin\ExamQuestion;
use Illuminate\Http\Request;

class ExamQuestionController extends Controller
{
    public function index(Exam $exam)
    {
        $exams = Exam::where('id', '!=', $exam->id)
            ->has('questions')
            ->get();
        return view('admin.exams.questions.index', compact('exam', 'exams'));
    }

    public function store(Request $request, Exam $exam)
    {
        $question = $exam->questions()->create([
            'question_text' => $request->question_text,
        ]);

        foreach ($request->answers as $i => $answer_text)
        {
            $question->answers()->create([
                'answer_text' => $answer_text,
                'is_true' => !empty($request->trueAnswer[$i])
            ]);
        }

        return back()->withInput()->with('status', 'Başarıyla eklendi!');
    }

    public function edit(Exam $exam, ExamQuestion $question)
    {
        return view('admin.exams.questions.edit', compact('exam', 'question'));
    }

    public function update(Request $request, Exam $exam, ExamQuestion $question)
    {
        $question->update([
            'question_text' => $request->question_text
        ]);

        foreach ($request->answers as $i => $answer_text)
        {
            $question->answers()->find($i)->update([
                'answer_text' => $answer_text,
                'is_true' => $request->trueAnswer == $i
            ]);
        }

        return response()->redirectToRoute('admin.exams.questions.index', $exam)->with('status', 'Başarıyla düzenlendi!');
    }

    public function destroy(Exam $exam, ExamQuestion $question)
    {
        $question->answers()->delete();
        $question->delete();

        return response()->json(['success' => true, 'message' => 'İşlem Başarılı']);
    }

    public function copy(Request $request, Exam $exam)
    {
        $fromExam = Exam::find($request->from);
        $fromExam->relations = [];
        $fromExam->load('questions');

        foreach ($fromExam->getRelations() as $questions) {
            foreach ($questions as $question)
            {
                $newQuestion = $exam->questions()->create(['question_text' => $question->question_text]);

                foreach ($question->answers as $answer) {
                    $newQuestion->answers()->create([
                        'answer_text' => $answer->answer_text,
                        'is_true' => $answer->is_true
                    ]);
                }
            }
        }

        return redirect()->back()->with('status', 'Başarıyla eklendi!');
    }
}
