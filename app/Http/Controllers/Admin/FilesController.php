<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Upload\File;
use Illuminate\Http\Request;
use Storage;
use Str;

class FilesController extends Controller
{
    public function store(Request $request)
    {
        $file = new File;

        $name = $request->file($request->attr)->getClientOriginalName();
        $extension = $request->file($request->attr)->extension();
        $path = $request->file($request->attr)->store('/', );

        $size = Storage::size($path);

        $file->name = $name;
        $file->path = Str::replaceFirst('public//', '', $path);
        $file->size = $size;
        $file->extension = $extension;

        if (!empty($request->modelId))
        {
            $model = $request->model::find($request->modelId);

            if (!$request->multiple)
            {
                if ($model->files->isNotEmpty())
                {
                    $model->files->first()->delete();
                }
            }

            $model->files()->save($file);
        }
        else
        {
            $file->save();
        }

        return response()->json([
            'success' => true,
            'result' => [
                'id' => $file->id
            ]
        ]);
    }
}
