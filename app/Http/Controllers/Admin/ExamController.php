<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreExamRequest;
use App\Models\Admin\Course;
use App\Models\Admin\Exam;

class ExamController extends Controller
{
    public function index()
    {
        $exams = Exam::all();
        $courses = Course::all();
        return view('admin.exams.index', ['exams' => $exams, 'courses' => $courses]);
    }

    public function store(StoreExamRequest $request)
    {
        $validatedData = $request->validated();

        $exam = new Exam($validatedData);
        $exam->save();

        return redirect()->route('admin.exams.instance.create', ['exam' => $exam]);
    }

    public function show(Exam $exam)
    {
        return view('admin.exams.show', compact('exam'));
    }

    public function edit(Exam $exam)
    {
        $courses = Course::all();
        return view('admin.exams.edit', ['exam' => $exam, 'courses' => $courses]);
    }

    public function update(StoreExamRequest $request, Exam $exam)
    {
        $validatedData = $request->validated();

        $exam->update($validatedData);

        return response()->redirectToRoute('admin.exams.index')->with('status', 'Başarıyla düzenlendi!');
    }

    public function destroy(Exam $exam)
    {
        $exam->delete();

        return response()->json(['success' => true, 'message' => 'İşlem Başarılı']);
    }
}
