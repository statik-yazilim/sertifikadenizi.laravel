<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreCourseRequest;
use App\Models\Admin\Course;
use App\Http\Controllers\Controller;
use App\Models\Upload\File;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::all();
        return view('admin.courses.index', ['courses' => $courses]);
    }

    public function create()
    {
        return view('admin.courses.create');
    }

    public function store(StoreCourseRequest $request)
    {
        $validatedData = $request->validated();

        $course = new Course($validatedData);
        $course->save();

        if (!empty($request->imageIds))
        {
            $file = File::find($request->imageIds);
            $course->files()->save($file);
        }

        return back()->withInput()->with('status', 'Başarıyla eklendi!');
    }

    public function show(Course $course)
    {
        //
    }

    public function edit(Course $course)
    {
        return view('admin.courses.edit', ['course' => $course]);
    }

    public function update(StoreCourseRequest $request, Course $course)
    {
        $validatedData = $request->validated();

        $course->update($validatedData);

        return response()->redirectToRoute('admin.courses.index')->with('status', 'Başarıyla düzenlendi!');
    }

    public function destroy(Course $course)
    {
        $course->delete();

        return response()->json(['success' => true, 'message' => 'İşlem Başarılı']);
    }
}
