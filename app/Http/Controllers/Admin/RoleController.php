<?php

namespace App\Http\Controllers\Admin;

use App\Models\PermissionCategory;
use App\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('admin.roles.index', ['roles' => $roles]);
    }

    public function create()
    {
        return view('admin.roles.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|unique:roles', 'description' => 'nullable']);

        $role = new Role();
        $role->name = $request->name;
        $role->description = $request->description;
        $role->guard_name = '*';
        $role->save();

        return back()->withInput()->with('status', 'Başarıyla eklendi!');
    }

    public function show(Role $role)
    {
        //
    }

    public function edit(Role $role)
    {
        return view('admin.roles.edit', ['role' => $role]);
    }

    public function update(Request $request, Role $role)
    {
        $this->validate($request, ['name' => 'required|unique:roles,name,' . $role->id, 'description' => 'nullable']);

        $role->update([
            'name' => $request->name,
            'description' => $request->description
        ]);

        return response()->redirectToRoute('admin.roles.index')->with('status', 'Başarıyla düzenlendi!');
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return response()->json(['success' => true, 'message' => 'İşlem Başarılı']);
    }

    public function permissions(Role $role)
    {
        $permission_categories = PermissionCategory::all();

        return view('admin.roles.permissions', ['role' => $role, 'permission_categories' => $permission_categories]);
    }

    public function storePermissions(Request $request, Role $role)
    {
        $permissions = $request->get('permissions');

        $role->syncPermissions($permissions);

        return response()->redirectToRoute('admin.roles.index')->with('status', 'Başarıyla düzenlendi!');
    }
}
