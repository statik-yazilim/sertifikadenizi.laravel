<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        if (!Auth::check())
            return redirect()->route('admin.login');
        else {
            if (!Auth::user()->hasRole('Admin'))
                return redirect('/');
        }

        return view('admin.home.index');
    }
}
