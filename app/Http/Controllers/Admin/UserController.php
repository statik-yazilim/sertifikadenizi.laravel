<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUserRequest;
use App\Models\Role;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        $roles = Role::all();
        return view('admin.users.index', ['users' => $users, 'roles' => $roles]);
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(StoreUserRequest $request)
    {
        $validatedData = $request->validated();

        $user = new User($validatedData);
        $user->save();

        $user->assignRole($validatedData['role']);

        return back()->with('status', 'Başarıyla eklendi!');
    }

    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        $roles = Role::all();
        return view('admin.users.edit', ['user' => $user, 'roles' => $roles]);
    }

    public function update(StoreUserRequest $request, User $user)
    {
        $validatedData = $request->validated();

        $user->update($validatedData);

        $user->syncRoles($validatedData['role']);

        return response()->redirectToRoute('admin.users.index')->with('status', 'Başarıyla düzenlendi!');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(['success' => true, 'message' => 'İşlem Başarılı']);
    }
}
