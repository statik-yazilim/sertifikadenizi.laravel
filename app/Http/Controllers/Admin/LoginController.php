<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('admin.auth.login');
    }

    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, true))
        {
            if (Auth::user()->hasRole('Admin'))
                return redirect()->route('admin.index');
            else
                return redirect()->back()->withInput()->withErrors(['auth_failed' => 'Kullanıcı bilgisi yanlış.']);
        }

        return redirect()->back()->withInput()->withErrors(['auth_failed' => 'Kullanıcı bilgisi yanlış.']);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('index');
    }
}
