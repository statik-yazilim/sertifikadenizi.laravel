<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;
use Str;

class CreateCrud extends Command
{
    protected $signature = 'statik:crud {model?} {--a|admin}';
    protected $description = 'Create CRUD Files';
    protected $customStubDir;
    protected $verbs = ['index', 'create', 'edit', 'show'];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (empty($this->argument('model')))
        {
            $model = $this->ask('Name of the model: ');
        }
        else
        {
            $model = $this->argument('model');
        }

        $table = Str::snake(Str::pluralStudly(class_basename($model)));

        $this->line('Creating model with name ' . $model);
        $this->callSilent('make:model', [
            'name' => ($this->option('admin') ? '\Admin\\' : '') . $model
        ]);
        $this->info('Done');

        $this->line('Creating controller ' . $model);
        $this->callSilent('statik:controller', [
            'name' => ($this->option('admin') ? '\Admin\\' : '') . $model . 'Controller',
            '-r',
            '--model' => ($this->option('admin') ? '\Admin\\' : '') . $model
        ]);
        $this->info('Done');

        $this->line('Creating migration for model');
        $this->callSilent('make:migration', [
            'name' => "create_{$table}_table",
            '--create' => $table,
        ]);
        $this->info('Done');

        $this->line('Creating form request for model');
        $this->callSilent('make:request', [
            'name' => ($this->option('admin') ? '\Admin\\' : '') . "Store{$model}Request",
        ]);
        $this->info('Done');

        $this->line('Creating blade files');

        $viewPath = config('view.paths')[0];
        if ($this->option('admin'))
        {
            $viewPath .= '\\admin';
        }
        $viewPath .= "\\{$table}";

        $this->createDir($viewPath);

        if ($this->option('admin'))
        {
            $files = [
                base_path('stubs/views/edit.blade.php') => 'edit',
                base_path('stubs/views/index.blade.php') => 'index',
            ];

            foreach ($files as $old => $new)
            {
                $newFile = $this->viewPath($new, $viewPath);

                if (File::exists($newFile))
                {
                    $this->error("File {$newFile} already exists!");
                }

                File::put($newFile, File::get($old));

                $this->info("{$new} done.");
            }
        }
        else
        {
            $files = array_map(function ($view) use ($table)
            {
                return $view;
            }, $this->verbs);

            foreach ($files as $file)
            {
                $newFile = $this->viewPath($file, $viewPath);

                if (File::exists($newFile))
                {
                    $this->error("File {$newFile} already exists!");
                }

                File::put($newFile, "merhaba");

                $this->info("{$file} done.");
            }
        }

        return 0;
    }

    public function viewPath($view, $path)
    {
        $view = str_replace('.', '\\', $view) . '.blade.php';

        return "{$path}\\{$view}";
    }


    public function createDir($path)
    {
        if (!file_exists($path))
        {
            mkdir($path, 0777, true);
        }
    }
}
