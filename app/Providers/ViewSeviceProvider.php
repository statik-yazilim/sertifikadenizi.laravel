<?php

namespace App\Providers;

use App\Models\Admin\Office;
use App\Models\Admin\Service;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewSeviceProvider extends ServiceProvider
{
    public function boot()
    {
        View::composer(['app', 'services.index', 'services.show'], function ($view) {
            // data to be shared between views
        });
    }
}
