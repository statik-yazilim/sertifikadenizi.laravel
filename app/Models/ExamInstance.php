<?php

namespace App\Models;

use App\Models\Admin\Exam;
use App\Models\Admin\ExamStudent;
use Illuminate\Database\Eloquent\Model;

class ExamInstance extends Model
{
    protected $guarded = ['id'];

    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function results()
    {
        return $this->hasMany(ExamResult::class);
    }

    public function currentUserResult()
    {
        $examResult = ExamResult::where('user_id', \Auth::id())
            ->where('exam_instance_id', $this->id)->get();

        return $examResult;
    }

    public function students()
    {
        return $this->hasMany(ExamStudent::class);
    }
}
