<?php

namespace App\Models;

use App\Models\Admin\Exam;
use App\Models\Admin\ExamStudent;
use App\Models\Admin\Video;
use Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes, HasRoles {
        hasPermissionTo as hasPermissionToOriginal;
    }
    protected $guard_name = '*';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'tc_no'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        if (!empty($value))
            $this->attributes['password'] = Hash::make($value);
    }


    public function hasPermissionTo($permission, $guardName = '*'): bool
    {
        return $this->hasPermissionToOriginal($permission, $guardName);
    }

    protected function getDefaultGuardName(): string
    {
        return '*';
    }

    public function videos()
    {
        return $this->belongsToMany(Video::class, 'video_histories')->using(VideoHistory::class)->withTimestamps();
    }

    public function didWatchVideo(Video $video)
    {
        return $this->videos()->find($video);
    }

    public function didJoinExam(Exam $exam)
    {
        $examInstance = ExamStudent::where([
            ['exam_instance_id', '=', $exam->instance->id],
            ['user_id', '=', $this->id]
        ])->exists();

        return $examInstance;
    }

    public function didUserPass()
    {
        $examResult = ExamResult::where('user_id', $this->id)
            ->where('is_success', 1)->exists();

        return $examResult;
    }

    public function scopeNotRole(Builder $query, $roles, $guard = null): Builder
    {
        if ($roles instanceof Collection) {
            $roles = $roles->all();
        }

        if (! is_array($roles)) {
            $roles = [$roles];
        }

        $roles = array_map(function ($role) use ($guard) {
            if ($role instanceof Role) {
                return $role;
            }

            $method = is_numeric($role) ? 'findById' : 'findByName';
            $guard = $guard ?: $this->getDefaultGuardName();

            return $this->getRoleClass()->{$method}($role, $guard);
        }, $roles);

        return $query->whereHas('roles', function ($query) use ($roles) {
            $query->where(function ($query) use ($roles) {
                foreach ($roles as $role) {
                    $query->where(config('permission.table_names.roles').'.id', '!=' , $role->id);
                }
            });
        });
    }
}
