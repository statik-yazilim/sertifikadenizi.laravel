<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class VideoHistory extends Pivot
{
    public $incrementing = true;
    protected $table = 'video_histories';
}
