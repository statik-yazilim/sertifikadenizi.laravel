<?php

namespace App\Models;

use App\Models\Admin\ExamAnswer;
use App\Models\Admin\ExamQuestion;
use Illuminate\Database\Eloquent\Model;

class ExamResultDetail extends Model
{
    public $incrementing = true;

    public function exam_result()
    {
        return $this->belongsTo(ExamResult::class);
    }

    public function question()
    {
        return $this->belongsTo(ExamQuestion::class, 'exam_question_id');
    }

    public function answer()
    {
        return $this->belongsTo(ExamAnswer::class, 'exam_customer_answer_id');
    }

    public function trueAnswer()
    {
        return $this->belongsTo(ExamAnswer::class, 'actual_true_answer_id');
    }
}
