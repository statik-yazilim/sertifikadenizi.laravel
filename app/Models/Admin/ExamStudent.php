<?php

namespace App\Models\Admin;

use App\Models\ExamInstance;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ExamStudent extends Model
{
    protected $guarded = ['id'];

    public function instance()
    {
        return $this->belongsTo(ExamInstance::class);
    }

    public function users()
    {
        return $this->hasManyThrough(ExamStudent::class, User::class);
    }
}
