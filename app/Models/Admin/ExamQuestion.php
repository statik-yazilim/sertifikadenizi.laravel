<?php

namespace App\Models\Admin;


use Illuminate\Database\Eloquent\Model;

class ExamQuestion extends Model
{
    protected $guarded = ['id'];

    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }

    public function answers()
    {
        return $this->hasMany(ExamAnswer::class)->orderBy('id');
    }
}
