<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ExamAnswer extends Model
{
    protected $guarded = ['id'];

    public function question()
    {
        return $this->belongsTo(ExamQuestion::class);
    }
}
