<?php

namespace App\Models\Admin;

use App\Traits\Uploadable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use Uploadable, SoftDeletes;

    protected $guarded = ['id'];

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function exams()
    {
        return $this->hasMany(Exam::class);
    }
}
