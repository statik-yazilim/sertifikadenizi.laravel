<?php

namespace App\Models\Admin;

use App\Models\User;
use App\Models\VideoHistory;
use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'video_histories')->using(VideoHistory::class)
            ->withTimestamps();
    }
}
