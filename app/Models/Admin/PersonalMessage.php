<?php

namespace App\Models\Admin;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersonalMessage extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function thread()
    {
        return $this->belongsTo(PersonalMessageThread::class, 'thread_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
