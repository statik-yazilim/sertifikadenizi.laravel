<?php

namespace App\Models\Admin;

use App\Models\ExamInstance;
use App\Models\ExamResult;
use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];
    protected $appends = ['status_text'];

    public function getStatusTextAttribute()
    {
        if ($this->status)
        {
            return 'Açık';
        }
        else
        {
            return 'Kapalı';
        }
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function questions()
    {
        return $this->hasMany(ExamQuestion::class)->orderBy('id');
    }

    public function instance()
    {
        return $this->hasOne(ExamInstance::class);
    }

    public function students()
    {
        return $this->hasManyThrough(ExamStudent::class, ExamInstance::class);
    }

    public function results()
    {
        return $this->hasManyThrough(ExamResult::class, ExamInstance::class);
    }

    public function currentUserResults()
    {
        return $this->hasManyThrough(ExamResult::class, ExamStudent::class, 'user_id', 'user_id');
    }
}
