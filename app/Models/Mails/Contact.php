<?php

namespace App\Models\Mails;

use App\Models\NonEloquentModel;

class Contact extends NonEloquentModel
{
    protected $guarded = [];
}
