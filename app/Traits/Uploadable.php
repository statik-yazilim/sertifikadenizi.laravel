<?php

namespace App\Traits;

use App\Models\Upload\File;

trait Uploadable
{
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }
}
