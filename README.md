# Statik WebCMS

## Installation

Copy .env.example to .env.
Update ```APP_NAME``` and ```APP_URL```
Update ```DB_*``` configurations.
Update ```MAIL_*``` configurations.

Update/install composer dependencies:
```bash
composer install
```

Generate Laravel application key:
```bash
php artisan key:generate
```

Migrate and seed the database:
```bash
php artisan migrate --seed
```

Link ```/storage``` to ```/public```:
```bash
php artisan storage:link
```

Clear caches:
```bash
php artisan cache:clear
php artisan config:clear
php artisan route:clear
php artisan view:clear
```

## Custom Artisan Commands

Create Model, CRUD Controller, Request, Migration and CRUD Views.
```bash
php artisan statik:crud ModelName
```
